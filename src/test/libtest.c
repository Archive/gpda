#include <glib.h>
#include <stdio.h>
#include <pdaapplication.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkvbox.h>


#include "pixbufs.h"

typedef struct {
  gint dummy;
} TestAppData;

gboolean
test_app_quit (PdaApplication* app)
{
  g_free (app->app_data);
  return TRUE;
}

PdaApplication *
test_app_start (PdaApplicationFactory *factory,
		PdaSessionObject *session_obj)
{
  PdaApplication *app;
  TestAppData *data;
  gchar buffer[100];
  static int num = 1;
  GtkWidget *vbox;
  GtkWidget *entry;
  GtkWidget *label;

  app = pda_application_new (factory);

  g_signal_connect_data (app, "quit", (GCallback)test_app_quit, NULL, NULL, FALSE);

  data = g_new (TestAppData, 1);
  app->app_data = data;

  vbox = gtk_vbox_new (FALSE, 0);
  
  sprintf (buffer, "Test app %d\n", num++);
  label = gtk_label_new (buffer);
  gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
  
  entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), entry, FALSE, FALSE, 0);
  
  app->toplevel_widget = vbox;

  gtk_widget_show_all (vbox);
  
  return app;
}



GdkPixbuf *
test_get_small_icon (PdaApplicationFactory *factory)
{
  static GdkPixbuf *pix = NULL;

  if (!pix)
    {
      pix = gdk_pixbuf_new_from_inline (sizeof (gmush_small), gmush_small, TRUE, NULL);
    }
  return gdk_pixbuf_ref (pix);
}

GdkPixbuf *
test_get_large_icon (PdaApplicationFactory *factory)
{
  static GdkPixbuf *pix = NULL;

  if (!pix)
    {
      pix = gdk_pixbuf_new_from_inline (sizeof (gmush_large), gmush_large, TRUE, NULL);
    }
  return gdk_pixbuf_ref (pix);
}

PdaApplicationFactory test_app_factory =
{
  "test app",
  0,
  FALSE,
  
  test_get_small_icon,
  test_get_large_icon,

  test_app_start
};

void register_application_factory (void)
{
  test_app_factory.app_category = pda_app_category_register ("test");
  pda_application_factory_register (&test_app_factory);
}
