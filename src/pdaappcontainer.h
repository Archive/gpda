/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef __PDA_APP_CONTAINER_H__
#define __PDA_APP_CONTAINER_H__


#include <gdk/gdk.h>
#include <gtk/gtkbin.h>
#include "pdaapplication.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define PDA_TYPE_APP_CONTAINER            (pda_app_container_get_type ())
#define PDA_APP_CONTAINER(obj)            (GTK_CHECK_CAST ((obj), PDA_TYPE_APP_CONTAINER, PdaAppContainer))
#define PDA_APP_CONTAINER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), PDA_TYPE_APP_CONTAINER, PdaAppContainerClass))
#define PDA_IS_APP_CONTAINER(obj)         (GTK_CHECK_TYPE ((obj), PDA_TYPE_APP_CONTAINER))
#define PDA_IS_APP_CONTAINER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), PDA_TYPE_APP_CONTAINER))
#define PDA_APP_CONTAINER_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), PDA_TYPE_APP_CONTAINER, PdaAppContainerClass))

typedef struct _PdaAppContainer       PdaAppContainer;
typedef struct _PdaAppContainerClass  PdaAppContainerClass;

struct _PdaAppContainer
{
  GtkBin bin;

  PdaApplication *cur_app;
  GList *apps;

  GtkAllocation child_allocation;
};

struct _PdaAppContainerClass
{
  GtkBinClass parent_class;

  void (*compute_child_allocation) (PdaAppContainer *frame, GtkAllocation *allocation);
};


GtkType    pda_app_container_get_type      (void) G_GNUC_CONST;
GtkWidget* pda_app_container_new           (void);

PdaApplication *pda_app_container_current  (PdaAppContainer *container);
void       pda_app_container_add_app       (PdaAppContainer *container, PdaApplication *app);
void       pda_app_container_remove_app    (PdaAppContainer *container, PdaApplication *app);
void       pda_app_container_switch_to_app (PdaAppContainer *container, PdaApplication *app);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __PDA_APP_CONTAINER_H__ */
