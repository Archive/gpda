#include <glib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
  int i, j;
  GList *items, *ltmp;

  for(i = 1, j = 0, items = NULL; i < argc; i++)
    {
      FILE *fh;
      char buf[128];

      fh = fopen(argv[i], "r");
      if(!fh)
	continue;

      while(fgets(buf, sizeof(buf), fh))
	{
	  g_strstrip(buf);

	  if(!buf[0] || buf[0] == '#')
	    continue;

	  items = g_list_prepend(items, g_strdup(buf));
	  j++;
	}

      fclose(fh);
    }

  /* This code appears funky because we traverse the list backwards */
  printf("static const GList lemma[] = {\n");
  for(ltmp = g_list_last(items), i = 0; ltmp; ltmp = ltmp->prev, i++)
    {
      printf("{ \"%s\", ", (char *)ltmp->data);

      if(ltmp->prev)
	printf("(GList *)&lemma[%d], ", i + 1);
      else
	printf("NULL, ");

      if(ltmp->next)
	printf("(GList *)&lemma[%d] ", i - 1);
      else
	printf("NULL ");
      printf("}%s\n", ltmp->prev?",":"");
    }
  printf("};\n");

  return 0;
}
