#include "gtkpeninput.h"
#include "gtkpeninputbutton.h"

#include "pixbufs.h"

#define POPUP_TIMEOUT 1000

enum {
  ARG_0,
  ARG_PI
};

static void gtk_pen_input_button_class_init(GtkPenInputClass *klass);
static void gtk_pen_input_button_init(GtkPenInputButton *pib);
static void gtk_pen_input_button_set_arg(GtkObject *obj, GtkArg *arg, guint arg_id);
static void gtk_pen_input_button_finalize(GObject *obj);
static gboolean gtk_pen_input_button_button_press_event(GtkWidget *w,
							GdkEventButton    *event);
static gboolean gtk_pen_input_button_button_release_event(GtkWidget *w,
							  GdkEventButton    *event);
static void gtk_pen_input_button_toggled(GtkToggleButton *btn);
static void gtk_pen_input_button_menu_set_mode(GtkWidget *w, GtkPenInputButton *pib);
static void gtk_pen_input_button_set_mode(GtkPenInputButton *pib, GtkPenInputMode mode);
static void popup_menu_detach (GtkWidget *attach_widget, GtkMenu *menu);
static void gtk_pen_input_button_size_request(GtkWidget *widget, GtkRequisition *req);

GType
gtk_pen_input_button_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkPenInputButtonClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_pen_input_button_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkPenInputButton),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_pen_input_button_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_TOGGLE_BUTTON,
					"GtkPenInputButton",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

static void
gtk_pen_input_button_class_init(GtkPenInputClass *klass)
{
  parent_class = g_type_class_peek_parent(klass);

  G_OBJECT_CLASS(klass)->finalize = gtk_pen_input_button_finalize;
  GTK_OBJECT_CLASS(klass)->set_arg = gtk_pen_input_button_set_arg;
  GTK_WIDGET_CLASS(klass)->button_press_event = gtk_pen_input_button_button_press_event;
  GTK_WIDGET_CLASS(klass)->button_release_event =
    gtk_pen_input_button_button_release_event;
  GTK_WIDGET_CLASS(klass)->size_request = gtk_pen_input_button_size_request;
  GTK_TOGGLE_BUTTON_CLASS(klass)->toggled = gtk_pen_input_button_toggled;
  gtk_object_add_arg_type ("GtkPenInputButton::pi", GTK_TYPE_PEN_INPUT,
			   GTK_ARG_WRITABLE|GTK_ARG_CONSTRUCT_ONLY,
			   ARG_PI);
}

static void
gtk_pen_input_button_init(GtkPenInputButton *pib)
{
  GtkWidget *choose_menu, *wtmp;
  GSList *group;

  pib->mode_img = gtk_widget_new(gtk_image_get_type(), NULL);
  gtk_container_add(GTK_CONTAINER(pib), pib->mode_img);
  gtk_widget_show(pib->mode_img);

  pib->choose_menu_timer = 1;
  pib->choose_menu = choose_menu = gtk_menu_new();
  group = NULL;

#define ADD_MI(label, cb) \
  wtmp = pib->mode_menu[cb] = gtk_radio_menu_item_new_with_label(group, label); \
  group = gtk_radio_menu_item_group(GTK_RADIO_MENU_ITEM(wtmp)); \
  gtk_widget_show(wtmp); \
  gtk_object_set_data(GTK_OBJECT(wtmp), "GtkPenInputButton::mode", \
		      GUINT_TO_POINTER(cb)); \
  gtk_signal_connect(GTK_OBJECT(wtmp), "activate", GTK_SIGNAL_FUNC(gtk_pen_input_button_menu_set_mode), pib); \
  gtk_container_add(GTK_CONTAINER(choose_menu), wtmp)

#ifdef USE_WORDPICK
  ADD_MI("Pickboard", GTK_PEN_INPUT_PICK);
#endif
  ADD_MI("Handwriting", GTK_PEN_INPUT_HANDWRITING);
  ADD_MI("Keyboard", GTK_PEN_INPUT_QWERTY);
  ADD_MI("Opti", GTK_PEN_INPUT_OPTI);
  ADD_MI("Unicode", GTK_PEN_INPUT_UNICODE);
  pib->choose_menu_timer = 0;

  gtk_menu_attach_to_widget(GTK_MENU(choose_menu), GTK_WIDGET(pib),
			    popup_menu_detach);
}

static void
gtk_pen_input_button_finalize(GObject *obj)
{
  parent_class->finalize(obj);
}

static void
gtk_pen_input_button_set_arg(GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GtkPenInputButton *pib = GTK_PEN_INPUT_BUTTON(obj);

  switch(arg_id)
    {
    case ARG_PI:
      pib->pi = GTK_PEN_INPUT(gtk_object_ref(GTK_VALUE_OBJECT(*arg)));
      /* TODO: connect to signals */
      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(pib), GTK_WIDGET_VISIBLE(pib->pi));
      if(pib->pi->mode == GTK_PEN_INPUT_LAST)
	gtk_pen_input_button_set_mode(pib, GTK_PEN_INPUT_OPTI);
      break;
    default:
      g_assert_not_reached();
      break;
    }
}

static void
gtk_pen_input_button_size_request(GtkWidget *widget, GtkRequisition *req)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->size_request)
    wclass->size_request(widget, req);
}

static void
popup_menu_detach (GtkWidget *attach_widget, GtkMenu   *menu)
{
  GTK_PEN_INPUT_BUTTON(attach_widget)->choose_menu = NULL;
}

static void
gtk_pen_input_button_position_menu(GtkMenu   *menu,
			    gint      *x,
			    gint      *y,
			    gboolean  *push_in,
			    gpointer   user_data)
{
  GtkWidget *w = (GtkWidget *)menu;
  GtkPenInput *pib = user_data;
  
  gdk_window_get_origin(GTK_WIDGET(pib)->window, x, y);
  if(GTK_WIDGET_NO_WINDOW(pib))
    {
      *x += GTK_WIDGET(pib)->allocation.x;
      *y += GTK_WIDGET(pib)->allocation.y;
    }

  *x = MIN(*x, gdk_screen_width());
  *y = MIN(*y, gdk_screen_height());
  *x = MAX(*x, w->requisition.width);
  *y = MAX(*y, w->requisition.height);
  *y -= w->requisition.height;
}

static gboolean
gtk_pen_input_button_popup_menu(gpointer data)
{
  GtkPenInputButton *pib = data;

  pib->choose_menu_timer = 0;
  gtk_menu_popup(GTK_MENU(pib->choose_menu), NULL, NULL,
		 gtk_pen_input_button_position_menu, pib,
		 666 /* Get GtkMenuShell to ignore the button release */,
		 GDK_CURRENT_TIME);

  return FALSE;
}

static gboolean
gtk_pen_input_button_button_press_event(GtkWidget         *w,
				 GdkEventButton    *event)
{
  GtkPenInputButton       *pib = GTK_PEN_INPUT_BUTTON(w);

  if(event->button != 1)
    return FALSE;

  if(!pib->choose_menu_timer)
    pib->choose_menu_timer = g_timeout_add(POPUP_TIMEOUT,
					  gtk_pen_input_button_popup_menu,
					  pib);

  return TRUE;
}

static gboolean
gtk_pen_input_button_button_release_event(GtkWidget         *w,
				   GdkEventButton    *event)
{
  GtkPenInputButton       *pib = GTK_PEN_INPUT_BUTTON(w);

  if(event->button != 1)
    return FALSE;

  if(pib->choose_menu_timer)
    {
      g_source_remove(pib->choose_menu_timer);
      pib->choose_menu_timer = 0;

      /* It was really pressed after all, so sorry about that. */
      gtk_button_pressed(GTK_BUTTON(pib));
      GTK_WIDGET_CLASS(parent_class)->button_release_event(w, event);
    }
  /* else they will have seen the menu */
  pib->choose_menu_timer = 0;

  return TRUE;
}

static void
gtk_pen_input_button_set_input_visibility(GtkPenInputButton *pib, gboolean vis)
{
  if(pib->pi->mode == GTK_PEN_INPUT_LAST) /* Nothing to see */
    return;

  if(vis)
    gtk_widget_show(GTK_WIDGET(pib->pi));
  else
    gtk_widget_hide(GTK_WIDGET(pib->pi));
}

static void
gtk_pen_input_button_toggled(GtkToggleButton *btn)
{
  GtkPenInputButton *pib = GTK_PEN_INPUT_BUTTON(btn);

  if(GTK_TOGGLE_BUTTON_CLASS(parent_class)->toggled)
    GTK_TOGGLE_BUTTON_CLASS(parent_class)->toggled(btn);

  if(pib->pi->mode != GTK_PEN_INPUT_LAST)
    gtk_pen_input_button_set_input_visibility(pib, btn->active);
}

static void
gtk_pen_input_button_set_mode(GtkPenInputButton *pib, GtkPenInputMode mode)
{
  static const char *mode_icon_data[] = {
    input_pick,
    input_qwerty,
    input_handwriting,
    input_opti,
    input_unicode
  };
  static const int mode_icon_size[] = {
    sizeof (input_pick),
    sizeof (input_qwerty),
    sizeof (input_handwriting),
    sizeof (input_opti),
    sizeof (input_unicode)
  };

  if(mode == pib->pi->mode)
    return;

  if(!pib->mode_icons[mode])
    pib->mode_icons[mode] =
      gdk_pixbuf_new_from_inline(mode_icon_size[mode], mode_icon_data[mode], TRUE, NULL);
  gtk_image_set_from_pixbuf(GTK_IMAGE(pib->mode_img), pib->mode_icons[mode]);

  gtk_pen_input_set_mode(pib->pi, mode);

  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(pib->mode_menu[mode]), TRUE);
}

static void
gtk_pen_input_button_menu_set_mode(GtkWidget *w, GtkPenInputButton *pib)
{
  GtkPenInputMode mode;

  if(!GTK_CHECK_MENU_ITEM(w)->active)
    return;
  
  mode = GPOINTER_TO_UINT(gtk_object_get_data(GTK_OBJECT(w),
					      "GtkPenInputButton::mode"));

  gtk_pen_input_button_set_mode(pib, mode);

  if(!pib->choose_menu_timer)
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(pib), TRUE);
}

GtkWidget *
gtk_pen_input_button_new(GtkPenInput *pi)
{
  return gtk_widget_new(gtk_pen_input_button_get_type(), "pi",
			pi, NULL);
}
