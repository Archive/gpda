#ifndef GTKPENINPUT_H
#define GTKPENINPUT_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_PEN_INPUT (gtk_pen_input_get_type())
#define GTK_PEN_INPUT(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_PEN_INPUT, GtkPenInput))
#define GTK_PEN_INPUT_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_PEN_INPUT, GtkPenInputClass))
#define GTK_IS_PEN_INPUT(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_PEN_INPUT))
#define GTK_IS_PEN_INPUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_PEN_INPUT))
#define GTK_PEN_INPUT_GET_CLASS(obj)  (G_CHECK_GET_CLASS ((obj), GTK_TYPE_PEN_INPUT, GtkPenInputClass))

typedef enum {
  GTK_PEN_INPUT_PICK,
  GTK_PEN_INPUT_QWERTY,
  GTK_PEN_INPUT_HANDWRITING,
  GTK_PEN_INPUT_OPTI,
  GTK_PEN_INPUT_UNICODE,
  GTK_PEN_INPUT_LAST
} GtkPenInputMode;

typedef struct {
  GtkVBox vbox;
  GtkPenInputMode mode;
  gboolean visible;
  GtkWidget *modes[GTK_PEN_INPUT_LAST];
  GtkWidget *completer;
} GtkPenInput;

typedef struct {
  GtkVBoxClass vbox_class;  
} GtkPenInputClass;

GType gtk_pen_input_get_type(void);
GtkWidget *gtk_pen_input_new(void);
void gtk_pen_input_set_mode(GtkPenInput *pi, GtkPenInputMode mode);
void gtk_pen_input_set_visible(GtkPenInput *pi, gboolean vis);

#endif
