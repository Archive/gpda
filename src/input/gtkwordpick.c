#include "gtkwordpick.h"
#include <string.h>

#define FONT_SIZE 12
#define SPACE_SIZE 4
#define WP_HEIGHT (FONT_SIZE + SPACE_SIZE*2)
#define BOX_SIZE (FONT_SIZE + SPACE_SIZE*2)
#define MARGIN_SIZE SPACE_SIZE*2
#define NUM_CHARS 1584

static void gtk_wordpick_class_init(GtkWordpickClass *klass);
static void gtk_wordpick_init(GtkWordpick *q);
static void gtk_wordpick_finalize(GObject *obj);
static void gtk_wordpick_size_request(GtkWidget *widget, GtkRequisition *req);
static void gtk_wordpick_size_allocate(GtkWidget *widget, GtkAllocation *al);
static gboolean gtk_wordpick_expose(GtkWidget *widget, GdkEventExpose *event);
static gboolean gtk_wordpick_button_press_event(GtkWidget *widget,
						GdkEventButton *event);
static gboolean gtk_wordpick_button_release_event(GtkWidget *widget,
						  GdkEventButton *event);

GType
gtk_wordpick_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkWordpickClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_wordpick_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkWordpick),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_wordpick_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					"GtkWordpick",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

enum {
  EMIT_KEY,
  EMIT_STRING,
  LAST_SIGNAL
};
static guint wordpick_signals[LAST_SIGNAL] = { 0 };

static void
gtk_wordpick_class_init(GtkWordpickClass *klass)
{
  GtkWidgetClass *wclass;

  parent_class = g_type_class_peek_parent(klass);
  G_OBJECT_CLASS(klass)->finalize = gtk_wordpick_finalize;
  wclass = GTK_WIDGET_CLASS(klass);
  wclass->button_press_event = gtk_wordpick_button_press_event;
  wclass->button_release_event = gtk_wordpick_button_release_event;
  wclass->size_request = gtk_wordpick_size_request;
  wclass->size_allocate = gtk_wordpick_size_allocate;
  wclass->expose_event = gtk_wordpick_expose;

  wordpick_signals[EMIT_KEY] =
    gtk_signal_new("emit_key",
		   GTK_RUN_FIRST,
		   GTK_CLASS_TYPE(wclass),
		   GTK_SIGNAL_OFFSET(GtkWordpickClass, emit_key),
		   g_cclosure_marshal_VOID__UINT,
		   GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
  wordpick_signals[EMIT_STRING] =
    gtk_signal_new("emit_string",
		   GTK_RUN_FIRST,
		   GTK_CLASS_TYPE(wclass),
		   GTK_SIGNAL_OFFSET(GtkWordpickClass, emit_string),
		   gtk_marshal_VOID__STRING,
		   GTK_TYPE_NONE, 1, GTK_TYPE_STRING);
}

static const char strsegs[][7] = {
  "[ABC]", "[DEF]", "[GHI]", "[JKL]", "[MNO]", "[PQR]", "[STU]",
  "[VWX]", "[YZ-']"
};
static const char drawstr[] = "ABC DEF GHI JKL MNO PQR STU VWX YZ-'";
static void
gtk_wordpick_init(GtkWordpick *u)
{
  GtkWordpickClass *klass;

  klass = GTK_WORDPICK_GET_CLASS(u);
  u->clicked_item.x = -1;
  u->clicked_item.y = -1;

  gtk_widget_add_events(GTK_WIDGET(u),
			GDK_EXPOSURE_MASK
			|GDK_BUTTON_PRESS_MASK
			|GDK_BUTTON_RELEASE_MASK);

  if(!klass->font)
    {
      PangoContext *ctxt;
      PangoFontDescription *fdes;
      PangoAnalysis anl;

      fdes = pango_font_description_new();
      pango_font_description_set_family(fdes,"Sans");
      pango_font_description_set_style(fdes,PANGO_STYLE_NORMAL);
      pango_font_description_set_variant(fdes,PANGO_VARIANT_NORMAL);
      pango_font_description_set_weight(fdes,PANGO_WEIGHT_NORMAL);
      pango_font_description_set_stretch(fdes,PANGO_STRETCH_NORMAL);
      pango_font_description_set_size(fdes,FONT_SIZE);

      ctxt = gtk_widget_get_pango_context(GTK_WIDGET(u));
      klass->font = pango_context_load_font(ctxt, fdes);
      klass->gstr = pango_glyph_string_new();

      anl.shape_engine = pango_font_find_shaper(klass->font, pango_language_from_string("C"),
						g_utf8_get_char("X"));
      anl.lang_engine = NULL;
      anl.font = klass->font;
      anl.level = 0;
      pango_shape(drawstr, sizeof(drawstr)-1, &anl, klass->gstr);
      //pango_font_description_free(fdes);
    }
}

static void
gtk_wordpick_finalize(GObject *obj)
{
  parent_class->finalize(obj);
}

static void
gtk_wordpick_size_request(GtkWidget *widget, GtkRequisition *req)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkWordpick *u = (GtkWordpick *)widget;
  int width, height;
  PangoRectangle irect;

  if(wclass->size_request)
    wclass->size_request(widget, req);

  pango_glyph_string_extents(GTK_WORDPICK_GET_CLASS(u)->gstr,
			     GTK_WORDPICK_GET_CLASS(u)->font,
			     NULL, &irect);
  width = PANGO_PIXELS(irect.width) + SPACE_SIZE*2;
  height = (FONT_SIZE + SPACE_SIZE*2);

  req->width = MAX(req->width, width);
  req->height = MAX(req->height, height);

  g_print("size_request %dx%d\n", req->width, req->height);
}

static void
gtk_wordpick_size_allocate(GtkWidget *widget, GtkAllocation *al)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->size_allocate)
    wclass->size_allocate(widget, al);
}

static void
gtk_wordpick_paint(GtkWidget *widget, GdkRectangle *area)
{
  GtkWordpick *u = (GtkWordpick *)widget;
  GtkWordpickClass *uc = GTK_WORDPICK_GET_CLASS(u);

  gdk_window_clear_area (widget->window, area->x, area->y,
			 area->width, area->height);

  gdk_draw_glyphs(widget->window, 
		  widget->style->text_gc[GTK_STATE_NORMAL],
		  uc->font, SPACE_SIZE, SPACE_SIZE + FONT_SIZE,
		  uc->gstr);
}

static gboolean
gtk_wordpick_expose (GtkWidget *widget, GdkEventExpose *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->expose_event)
    wclass->expose_event(widget, event);

  if(GTK_WIDGET_DRAWABLE(widget))
     gtk_wordpick_paint(widget, &event->area);

  return TRUE;
}

static gboolean
gtk_wordpick_button_press_event(GtkWidget *widget, GdkEventButton *event)
{
  GtkWordpick *u = (GtkWordpick *)widget;

  if(GTK_WIDGET_CLASS(parent_class)->button_press_event)
    GTK_WIDGET_CLASS(parent_class)->button_press_event(widget, event);

  u->clicked_item.x = event->x;
  u->clicked_item.y = event->y;
  
  return TRUE;
}

static int
idx_to_seg(int idx)
{
  switch(idx)
    {
    case 0:
    case 1:
    case 2:
      return 0; /* ABC */
    case 4:
    case 5:
    case 6:
      return 1; /* DEF */
    case 8:
    case 9:
    case 10:
      return 2; /* GHI */
    case 12:
    case 13:
    case 14:
      return 3; /* JKL */
    case 16:
    case 17:
    case 18:
      return 4; /* MNO */
    case 20:
    case 21:
    case 22:
      return 5; /* PQR */
    case 24:
    case 25:
    case 26:
      return 6; /* STU */
    case 28:
    case 29:
    case 30:
      return 7; /* VWX */
    case 32:
    case 33:
    case 34:
      return 8; /* YZ-' */
    }

  return -1;
}

static gboolean
gtk_wordpick_button_release_event(GtkWidget *widget, GdkEventButton *event)
{
  GtkWordpick *u = (GtkWordpick *)widget;
  GtkWordpickClass *uc = GTK_WORDPICK_GET_CLASS(widget);
  PangoAnalysis anl;
  int idx_press, idx_release, seg_press, seg_release;

  if(GTK_WIDGET_CLASS(parent_class)->button_release_event)
    GTK_WIDGET_CLASS(parent_class)->button_release_event(widget, event);

  if(u->clicked_item.y > WP_HEIGHT
     || event->y > WP_HEIGHT)
    return TRUE;

  anl.shape_engine = pango_font_find_shaper(uc->font, pango_language_from_string("C"),
					    g_utf8_get_char("X"));
  anl.lang_engine = NULL;
  anl.font = uc->font;
  anl.level = 0;

  pango_glyph_string_x_to_index(uc->gstr, (char *)drawstr,
				sizeof(drawstr) - 1,
				&anl,
				(u->clicked_item.x - SPACE_SIZE)*PANGO_SCALE,
				&idx_press, NULL);
  pango_glyph_string_x_to_index(uc->gstr, (char *)drawstr,
				sizeof(drawstr) - 1,
				&anl,
				(event->x - SPACE_SIZE)*PANGO_SCALE,
				&idx_release, NULL);
  seg_press = idx_to_seg(idx_press);
  seg_release = idx_to_seg(idx_release);
  if(seg_press == seg_release)
    {
      if(seg_press > 0)
	gtk_signal_emit(GTK_OBJECT(u), wordpick_signals[EMIT_STRING],
			strsegs[seg_release]);
      else
	gtk_signal_emit(GTK_OBJECT(u), wordpick_signals[EMIT_KEY],
			(guint)(' '));
    }
  u->clicked_item.x = -1;
  u->clicked_item.y = -1;

  return TRUE;
}

GtkWidget *
gtk_wordpick_new(void)
{
  return gtk_widget_new(gtk_wordpick_get_type(), NULL);
}
