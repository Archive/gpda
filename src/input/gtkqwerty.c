#include "gtkqwerty.h"
#include <string.h>
#include <gdk/gdkkeysyms.h>

#define DEFAULT_ROW_HEIGHT 16
#define DEFAULT_COL_WIDTH 10
#define SPACE_WIDTH 2
#define SPACE_HEIGHT 2

struct _GtkKeyDescriptionInternal {
  PangoGlyphString *gstr;
  guint8 pressed;
};

const GtkKeyDescription gtk_qwerty_opti_keys[] = {
  {"j", 'j', 0, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"p", 'p', 2, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"v", 'v', 4, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"g", 'g', 6, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"l", 'l', 8, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"y", 'y', 10, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"Bksp", GDK_BackSpace, 12, 4, 0,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_MODIFIER_STYLE },

  {"J", 'J', 0, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"P", 'P', 2, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"V", 'V', 4, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"G", 'G', 6, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"L", 'L', 8, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"Y", 'Y', 10, 2, 0, GTK_KEY_SHOW_SHIFTED },

  {"Caps", GDK_Caps_Lock, 0, 4, 1,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_SHIFT|GTK_KEY_HOLD_SHIFT|GTK_KEY_MODIFIER_STYLE },
  {"o", 'o', 4, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"t", 't', 6, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"h", 'h', 8, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"Ret", GDK_Return, 10, 4, 1,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_MODIFIER_STYLE },

  {"O", 'O', 4, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"T", 'T', 6, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"H", 'H', 8, 2, 1, GTK_KEY_SHOW_SHIFTED },

  {"b", 'b', 0, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"s", 's', 2, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"r", 'r', 4, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"e", 'e', 6, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"a", 'a', 8, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"w", 'w', 10, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"x", 'x', 12, 2, 2, GTK_KEY_SHOW_NORMAL },

  {"B", 'B', 0, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"S", 'S', 2, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"R", 'R', 4, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"E", 'E', 6, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"A", 'A', 8, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"W", 'W', 10, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"X", 'X', 12, 2, 2, GTK_KEY_SHOW_SHIFTED },

  {"Ctrl", GDK_Control_L, 0, 4, 3,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_CTRL|GTK_KEY_MODIFIER_STYLE },
  {"i", 'i', 4, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"n", 'n', 6, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"d", 'd', 8, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"Alt", GDK_Alt_L, 10, 4, 3,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_ALT|GTK_KEY_MODIFIER_STYLE },

  {"I", 'I', 4, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"N", 'N', 6, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"D", 'D', 8, 2, 3, GTK_KEY_SHOW_SHIFTED },

  {"q", 'q', 0, 2, 4, GTK_KEY_SHOW_NORMAL },
  {"f", 'f', 2, 2, 4, GTK_KEY_SHOW_NORMAL },
  {"u", 'u', 4, 2, 4, GTK_KEY_SHOW_NORMAL },
  {"m", 'm', 6, 2, 4, GTK_KEY_SHOW_NORMAL },
  {"c", 'c', 8, 2, 4, GTK_KEY_SHOW_NORMAL },
  {"k", 'k', 10, 2, 4, GTK_KEY_SHOW_NORMAL },
  {"z", 'z', 12, 2, 4, GTK_KEY_SHOW_NORMAL },

  {"Q", 'Q', 0, 2, 4, GTK_KEY_SHOW_SHIFTED },
  {"F", 'F', 2, 2, 4, GTK_KEY_SHOW_SHIFTED },
  {"U", 'U', 4, 2, 4, GTK_KEY_SHOW_SHIFTED },
  {"M", 'M', 6, 2, 4, GTK_KEY_SHOW_SHIFTED },
  {"C", 'C', 8, 2, 4, GTK_KEY_SHOW_SHIFTED },
  {"K", 'K', 10, 2, 4, GTK_KEY_SHOW_SHIFTED },
  {"Z", 'Z', 12, 2, 4, GTK_KEY_SHOW_SHIFTED },

  {".", '.', 14, 2, 1,
   GTK_KEY_SHOW_SHIFTED|GTK_KEY_SHOW_NORMAL|GTK_KEY_MODIFIER_STYLE},
  {" ", ' ', 14, 2, 2,
   GTK_KEY_SHOW_SHIFTED|GTK_KEY_SHOW_NORMAL},
  {",", ',', 14, 2, 3,
   GTK_KEY_SHOW_SHIFTED|GTK_KEY_SHOW_NORMAL|GTK_KEY_MODIFIER_STYLE},

  {NULL}
};

const GtkKeyDescription gtk_qwerty_keys[] = {
  {"`", '`', 0, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"1", '1', 2, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"2", '2', 4, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"3", '3', 6, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"4", '4', 8, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"5", '5', 10, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"6", '6', 12, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"7", '7', 14, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"8", '8', 16, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"9", '9', 18, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"0", '0', 20, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"-", '-', 22, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"=", '=', 24, 2, 0, GTK_KEY_SHOW_NORMAL },
  {"Bksp", GDK_BackSpace, 26, 4, 0,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_MODIFIER_STYLE },
  {"Tab", GDK_Tab, 0, 3, 1,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_MODIFIER_STYLE },
  {"q", 'q', 3, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"w", 'w', 5, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"e", 'e', 7, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"r", 'r', 9, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"t", 't', 11, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"y", 'y', 13, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"u", 'u', 15, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"i", 'i', 17, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"o", 'o', 19, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"p", 'p', 21, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"[", '[', 23, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"]", ']', 25, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"\\", '\\', 27, 2, 1, GTK_KEY_SHOW_NORMAL },
  {"Caps", GDK_Caps_Lock, 0, 4, 2,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_SHIFT|GTK_KEY_HOLD_SHIFT|GTK_KEY_MODIFIER_STYLE },
  {"a", 'a', 4, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"s", 's', 6, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"d", 'd', 8, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"f", 'f', 10, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"g", 'g', 12, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"h", 'h', 14, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"j", 'j', 16, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"k", 'k', 18, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"l", 'l', 20, 2, 2, GTK_KEY_SHOW_NORMAL },
  {";", ';', 22, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"'", '\'', 24, 2, 2, GTK_KEY_SHOW_NORMAL },
  {"Ret", GDK_Return, 26, 4, 2,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_MODIFIER_STYLE },
  {"Shift", GDK_Shift_L, 0, 5, 3,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_SHIFT|GTK_KEY_MODIFIER_STYLE },
  {"z", 'z', 5, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"x", 'x', 7, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"c", 'c', 9, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"v", 'v', 11, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"b", 'b', 13, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"n", 'n', 15, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"m", 'm', 17, 2, 3, GTK_KEY_SHOW_NORMAL },
  {",", ',', 19, 2, 3, GTK_KEY_SHOW_NORMAL },
  {".", '.', 21, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"/", '/', 23, 2, 3, GTK_KEY_SHOW_NORMAL },
  {"Shift", GDK_Shift_R, 25, 5, 3,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_SHIFT|GTK_KEY_MODIFIER_STYLE },
  {"Ctrl", GDK_Control_L, 0, 3, 4,
   GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_CTRL|GTK_KEY_MODIFIER_STYLE },
  {"Alt", GDK_Alt_L, 3, 3, 4, GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_ALT|GTK_KEY_MODIFIER_STYLE },
  {" ", ' ', 6, 18, 4, GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED },
  {"Alt", GDK_Alt_R, 24, 3, 4, GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_ALT|GTK_KEY_MODIFIER_STYLE },
  {"Ctrl", GDK_Control_R, 27, 3, 4, GTK_KEY_SHOW_NORMAL|GTK_KEY_SHOW_SHIFTED|GTK_KEY_TOGGLES_CTRL|GTK_KEY_MODIFIER_STYLE },

  {"~", '~', 0, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"!", '!', 2, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"@", '@', 4, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"#", '#', 6, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"$", '$', 8, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"%", '%', 10, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"^", '^', 12, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"&", '&', 14, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"*", '*', 16, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"(", '(', 18, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {")", ')', 20, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"_", '_', 22, 2, 0, GTK_KEY_SHOW_SHIFTED },
  {"+", '+', 24, 2, 0, GTK_KEY_SHOW_SHIFTED },

  {"Q", 'Q', 3, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"W", 'W', 5, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"E", 'E', 7, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"R", 'R', 9, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"T", 'T', 11, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"Y", 'Y', 13, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"U", 'U', 15, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"I", 'I', 17, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"O", 'O', 19, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"P", 'P', 21, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"{", '{', 23, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"}", '}', 25, 2, 1, GTK_KEY_SHOW_SHIFTED },
  {"|", '|', 27, 2, 1, GTK_KEY_SHOW_SHIFTED },

  {"A", 'A', 4, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"S", 'S', 6, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"D", 'D', 8, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"F", 'F', 10, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"G", 'G', 12, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"H", 'H', 14, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"J", 'J', 16, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"K", 'K', 18, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"L", 'L', 20, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {":", ':', 22, 2, 2, GTK_KEY_SHOW_SHIFTED },
  {"\"", '"', 24, 2, 2, GTK_KEY_SHOW_SHIFTED },

  {"Z", 'Z', 5, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"X", 'X', 7, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"C", 'C', 9, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"V", 'V', 11, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"B", 'B', 13, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"N", 'N', 15, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"M", 'M', 17, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"<", '<', 19, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {">", '>', 21, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {"?", '?', 23, 2, 3, GTK_KEY_SHOW_SHIFTED },
  {NULL}
};

static void gtk_qwerty_class_init(GtkQwertyClass *klass);
static void gtk_qwerty_init(GtkQwerty *q);
static void gtk_qwerty_finalize(GObject *obj);
static void gtk_qwerty_size_request(GtkWidget *widget, GtkRequisition *req);
static void gtk_qwerty_size_allocate(GtkWidget *widget, GtkAllocation *al);
static gboolean gtk_qwerty_expose (GtkWidget *widget, GdkEventExpose *event);
static gboolean gtk_qwerty_button_press_event(GtkWidget *widget, GdkEventButton *event);
static gboolean gtk_qwerty_button_release_event(GtkWidget *widget, GdkEventButton *event);
//static int row_to_y(GtkQwerty *q, int row);
static int col_to_x(GtkQwerty *q, int col);
static int y_to_row(GtkQwerty *q, int y);
static int x_to_col(GtkQwerty *q, int x);

enum {
  EMIT_KEY,
  LAST_SIGNAL
};
static guint qwerty_signals[LAST_SIGNAL] = { 0 };

GType
gtk_qwerty_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkQwertyClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_qwerty_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkQwerty),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_qwerty_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					"GtkQwerty",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

static void
gtk_qwerty_class_init(GtkQwertyClass *klass)
{
  GtkWidgetClass *wclass;
  parent_class = g_type_class_peek_parent(klass);
  G_OBJECT_CLASS(klass)->finalize = gtk_qwerty_finalize;
  wclass = GTK_WIDGET_CLASS(klass);
  wclass->button_press_event = gtk_qwerty_button_press_event;
  wclass->button_release_event = gtk_qwerty_button_release_event;
  wclass->size_request = gtk_qwerty_size_request;
  wclass->size_allocate = gtk_qwerty_size_allocate;
  wclass->expose_event = gtk_qwerty_expose;

  qwerty_signals[EMIT_KEY] =
    gtk_signal_new("emit_key",
		   GTK_RUN_FIRST,
		   GTK_CLASS_TYPE(wclass),
		   GTK_SIGNAL_OFFSET(GtkQwertyClass, emit_key),
		   g_cclosure_marshal_VOID__UINT,
		   GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
}

static void
gtk_qwerty_init(GtkQwerty *q)
{
  q->fdes = pango_font_description_new();
  pango_font_description_set_family(q->fdes,"Sans");
  pango_font_description_set_style(q->fdes,PANGO_STYLE_NORMAL);
  pango_font_description_set_variant(q->fdes,PANGO_VARIANT_NORMAL);
  pango_font_description_set_weight(q->fdes,PANGO_WEIGHT_NORMAL);
  pango_font_description_set_stretch(q->fdes,PANGO_STRETCH_NORMAL);
  pango_font_description_set_size(q->fdes,8*PANGO_SCALE);
  gtk_widget_add_events(GTK_WIDGET(q),
			GDK_EXPOSURE_MASK
			|GDK_BUTTON_PRESS_MASK
			|GDK_BUTTON_RELEASE_MASK);
  q->key_flags = 0;
}

static void
gtk_qwerty_finalize(GObject *obj)
{
  GtkQwerty *q = GTK_QWERTY(obj);
  g_free(q->key_int); q->key_int = NULL;

  if(q->fdes) {
    pango_font_description_free(q->fdes);
    q->fdes = NULL;
  }

  parent_class->finalize(obj);
}

static void
gtk_qwerty_size_allocate(GtkWidget *widget, GtkAllocation *al)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkQwerty *q = GTK_QWERTY(widget);
  int new_size, col_width;

  if(wclass->size_allocate)
    wclass->size_allocate(GTK_WIDGET(q), al);
  q->row_height = al->height / q->num_rows;
  col_width = al->width / q->num_columns;
  
  q->row_height = MIN(q->row_height, (col_width*4)/3);
  new_size = (q->row_height-SPACE_HEIGHT*2)*PANGO_SCALE;
  if(new_size != pango_font_description_get_size(q->fdes) || !q->font)
    {
      PangoContext *ctxt;

      if(q->font)
	g_object_unref(G_OBJECT(q->font));
      pango_font_description_set_size(q->fdes,new_size);
      ctxt = gtk_widget_get_pango_context(GTK_WIDGET(q));
      q->font = pango_context_load_font(ctxt, q->fdes);
    }
}

static void
gtk_qwerty_size_request(GtkWidget *widget, GtkRequisition *req)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkQwerty *q = GTK_QWERTY(widget);

  if(wclass->size_request)
    wclass->size_request(widget, req);

#ifdef NOT_ALEX_HACK
  if(q->font)
    {
      req->width = (PANGO_PIXELS(q->fdes.size))*q->num_columns;
      req->height = (SPACE_HEIGHT*2 + PANGO_PIXELS(q->fdes.size))*q->num_rows;
    }
  else
    {
      req->width = DEFAULT_COL_WIDTH*q->num_columns;
      req->height = DEFAULT_ROW_HEIGHT*q->num_rows;
    }
#else
  req->width = DEFAULT_COL_WIDTH*q->num_columns;
  req->height = DEFAULT_ROW_HEIGHT*q->num_rows;
#endif
}

static void
gtk_qwerty_paint(GtkWidget *widget, GdkRectangle *area)
{
  int row_height;
  int i;
  PangoAnalysis anl;
  PangoRectangle log_rect;
  gboolean shifted;

  GtkQwerty *q = GTK_QWERTY(widget);

  if(!GTK_WIDGET_DRAWABLE(widget))
    return;

  gdk_window_clear_area (widget->window, area->x, area->y,
			 area->width, area->height);

  row_height = q->row_height;
  anl.shape_engine = NULL;
  anl.lang_engine = NULL;
  anl.font = q->font;
  anl.level = 0;
  shifted = (q->key_flags & GTK_KEY_TOGGLES_SHIFT)?TRUE:FALSE;
  for(i = 0; i < q->num_rows; i++)
    {
      int j;

      /* It may be more efficient to just go through and paint them all... */
      if(area)
	{
	  if((row_height * i) > (area->y + area->height))
	    break;
	  if((row_height * (i+1)) < area->y)
	    continue;
	}

      for(j = 0; j < q->num_keys; j++)
	{
	  guint pixel_pos;
	  guint8 thiskey_flags;
	  GtkStateType boxstate;
	  GtkShadowType boxshadow;

	  if(q->key_info[j].row != i)
	    continue;
	  if((shifted
	      && !(q->key_info[j].flags & GTK_KEY_SHOW_SHIFTED))
	     || (!shifted
		 && !(q->key_info[j].flags & GTK_KEY_SHOW_NORMAL)))
	    continue;

	  thiskey_flags = q->key_info[j].flags;
	  pixel_pos = col_to_x(q, q->key_info[j].position);

	  /* Try doing some juju to get things looking nice. */
	  if(q->key_int[j].pressed)
	    {
	      boxstate = GTK_STATE_PRELIGHT;
	      boxshadow = GTK_SHADOW_ETCHED_IN;
	    }
	  else if(thiskey_flags & GTK_KEY_MODIFIER_STYLE)
	    {
	      boxstate = GTK_STATE_ACTIVE;
	      boxshadow = GTK_SHADOW_ETCHED_OUT;
	    }
	  else
	    {
	      boxstate = GTK_STATE_NORMAL;
	      boxshadow = GTK_SHADOW_OUT;
	    }
	  gtk_paint_box(widget->style, widget->window,
			boxstate, boxshadow,
			area, widget, NULL,
			pixel_pos, row_height * i,
			col_to_x(q, q->key_info[j].width),
			row_height);
	  if(!q->key_int[j].gstr)
	    {
	      if(!anl.shape_engine)
		anl.shape_engine = pango_font_find_shaper(q->font, pango_language_from_string("C"),
							  g_utf8_get_char("X"));
	      q->key_int[j].gstr = pango_glyph_string_new();
	      pango_shape(q->key_info[j].cstr,
			  strlen(q->key_info[j].cstr),
			  &anl, q->key_int[j].gstr);
	    }

	  pango_glyph_string_extents(q->key_int[j].gstr, q->font,
				     NULL, &log_rect);
	  gdk_draw_glyphs(widget->window,
			  widget->style->text_gc[GTK_STATE_NORMAL],
			  q->font,
			  pixel_pos + (col_to_x(q, q->key_info[j].width) - PANGO_PIXELS(log_rect.width))/2 - 1,
			  row_height * (i+1) - PANGO_PIXELS (PANGO_DESCENT (log_rect)) - SPACE_HEIGHT,
			  q->key_int[j].gstr);
	}
    }
}

static gboolean
gtk_qwerty_expose (GtkWidget *widget, GdkEventExpose *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->expose_event)
    wclass->expose_event(widget, event);

  if(GTK_WIDGET_DRAWABLE(widget))
     gtk_qwerty_paint(widget, &event->area);

  return TRUE;
}

static void
gtk_qwerty_handle_button(GtkWidget *widget, GdkEventButton *event)
{
  int row, pos, realrow, realpos;
  int j;
  GtkQwerty *q = GTK_QWERTY(widget);
  gboolean shifted = (q->key_flags & GTK_KEY_TOGGLES_SHIFT)?TRUE:FALSE;
  gboolean unset_modifiers = FALSE;
  gboolean is_click = TRUE;
  gdouble evx, evy;

  if(event->type == GDK_BUTTON_RELEASE)
    {
      evx = q->press_loc.x;
      evy = q->press_loc.y;
    }
  else
    {
      q->press_loc.x = evx = event->x;
      q->press_loc.y = evy = event->y;
    }

  row = y_to_row(q, evy);
  realrow = y_to_row(q, event->y);
  pos = x_to_col(q, evx);
  if(realrow != row)
    is_click = FALSE;

  for(j = 0; j < q->num_keys; j++)
    {
      if((shifted
	  && !(q->key_info[j].flags & GTK_KEY_SHOW_SHIFTED))
	 || (!shifted
	     && !(q->key_info[j].flags & GTK_KEY_SHOW_NORMAL)))
	continue;

      if(q->key_info[j].row == row
	 && pos >= q->key_info[j].position
	 && pos < (q->key_info[j].position + q->key_info[j].width))
	break;
    }

  if(j >= q->num_keys)
    return;

  realpos = x_to_col(q, event->x);
  if(realpos < q->key_info[j].position
     || pos >= (q->key_info[j].position + q->key_info[j].width))
    is_click = FALSE;

  if(q->key_info[j].flags & GTK_KEY_TOGGLES_MASK)
    {
      if(event->type == GDK_BUTTON_RELEASE && is_click)
	{
	  q->key_int[j].pressed ^= 1;
	  if(q->key_int[j].pressed)
	    q->key_flags |= (q->key_info[j].flags & GTK_KEY_TOGGLES_MASK);
	  else
	    q->key_flags &= ~(q->key_info[j].flags & GTK_KEY_TOGGLES_MASK);
	}
    }
  else
    {
      q->key_int[j].pressed = (event->type==GDK_BUTTON_PRESS)?1:0;

      if(event->type == GDK_BUTTON_RELEASE)
	unset_modifiers = is_click;
    }

  if(is_click && event->type == GDK_BUTTON_RELEASE)
    {
      if(event->type==GDK_BUTTON_RELEASE)
	gtk_signal_emit(GTK_OBJECT(q), qwerty_signals[EMIT_KEY],
			q->key_info[j].keyval);
    }

  gtk_widget_queue_draw(widget);

  if(unset_modifiers)
    {
      guint8 unset_mask;
      int k;

      /* Unset modifiers as necessary */
      unset_mask = GTK_KEY_TOGGLES_MASK & ~GTK_KEY_HOLD_SHIFT;
      if(q->key_flags & GTK_KEY_HOLD_SHIFT)
	unset_mask &= ~GTK_KEY_TOGGLES_SHIFT;
      for(k = 0; k < q->num_keys; k++)
	{
	  if(q->key_info[k].flags & unset_mask)
	    q->key_int[k].pressed = 0;
	}
      q->key_flags &= ~unset_mask;
    }
}

static gboolean
gtk_qwerty_button_press_event(GtkWidget *widget, GdkEventButton *event)
{
  if(event->button == 1)
    {
      switch(event->type)
	{
	case GDK_3BUTTON_PRESS:
	  event->type = GDK_BUTTON_PRESS;
	  gtk_qwerty_handle_button(widget, event);
	  event->type = GDK_BUTTON_RELEASE;
	  gtk_qwerty_handle_button(widget, event);
	case GDK_2BUTTON_PRESS:
	  event->type = GDK_BUTTON_PRESS;
	  gtk_qwerty_handle_button(widget, event);
	  event->type = GDK_BUTTON_RELEASE;
	  gtk_qwerty_handle_button(widget, event);
	  event->type = GDK_BUTTON_PRESS;
	  gtk_qwerty_handle_button(widget, event);
	  event->type = GDK_BUTTON_RELEASE;
	  gtk_qwerty_handle_button(widget, event);
	  break;
	default:
	  gtk_qwerty_handle_button(widget, event);
	  break;
	}
    }

  return TRUE;
}

static gboolean
gtk_qwerty_button_release_event(GtkWidget *widget, GdkEventButton *event)
{
  if(event->button == 1)
    gtk_qwerty_handle_button(widget, event);
  return TRUE;
}

void
gtk_qwerty_set_key_info(GtkQwerty *q, const GtkKeyDescription *key_info)
{
  int i;

  if(!key_info)
    key_info = gtk_qwerty_keys;

  for(i = 0; i < q->num_keys; i++)
    pango_glyph_string_free(q->key_int[i].gstr);

  q->num_columns = 0;
  q->num_rows = 0;
  for(i = 0; key_info[i].cstr; i++)
    {
      register int itmp = key_info[i].position+key_info[i].width;
      q->num_rows = MAX(q->num_rows, key_info[i].row);
      q->num_columns = MAX(q->num_columns, itmp );
    }
  q->num_rows++;
  q->num_columns++;

  q->num_keys = i;
  q->key_info = key_info;
  g_free(q->key_int);
  q->key_int = g_new0(GtkKeyDescriptionInternal, i);
}

GtkWidget *
gtk_qwerty_new(const GtkKeyDescription *key_info)
{
  GtkWidget *retval;
  retval = gtk_widget_new(gtk_qwerty_get_type(), NULL);

  gtk_qwerty_set_key_info(GTK_QWERTY(retval), key_info);
  
  return retval;
}

/*
static int
row_to_y(GtkQwerty *q, int row)
{
  return row * q->row_height;
}
*/

static int
col_to_x(GtkQwerty *q, int col)
{
  return (col * GTK_WIDGET(q)->allocation.width)/q->num_columns;
}

static int
y_to_row(GtkQwerty *q, int y)
{
  return y / q->row_height;
}

static int
x_to_col(GtkQwerty *q, int x)
{
  return (x * q->num_columns)/GTK_WIDGET(q)->allocation.width;
}
