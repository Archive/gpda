#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/Xlib.h>

/* Likewise */
gint gdk_color_white     (GdkColormap   *colormap,
                          GdkColor      *color);

static GdkFilterReturn
handle_ev(GdkXEvent *xevent, GdkEvent *event, gpointer data)
{
  XEvent *xev = (XEvent *)xevent;
  static GdkGC *gc = NULL;
  static int prevx = -1, prevy = -1;
  static int buttons = 0;

  if(xev->type == ButtonPress) buttons++;
  if(xev->type == ButtonRelease) buttons--;

  if(xev->type != MotionNotify)
    return GDK_FILTER_CONTINUE;

  if(!gc)
    {
      GdkGCValues vals;
      vals.subwindow_mode = GDK_INCLUDE_INFERIORS;
      gdk_color_white(gdk_colormap_get_system(), &vals.foreground);
      gdk_color_white(gdk_colormap_get_system(), &vals.background);
      gc = gdk_gc_new_with_values(GDK_ROOT_PARENT(),
				  &vals,
				  GDK_GC_SUBWINDOW
				  |GDK_GC_FOREGROUND
				  |GDK_GC_BACKGROUND);
    }

  if(prevx >= 0 && buttons)
    gdk_draw_line(GDK_ROOT_PARENT(), gc, prevx, prevy, xev->xmotion.x_root, xev->xmotion.y_root);
  prevx = xev->xmotion.x_root;
  prevy = xev->xmotion.y_root;

  return GDK_FILTER_REMOVE;
}

int main(int argc, char *argv[])
{
  gtk_init(&argc, &argv);
  gdk_window_add_filter(GDK_ROOT_PARENT(), handle_ev, NULL);
  gdk_pointer_grab(GDK_ROOT_PARENT(), FALSE, GDK_POINTER_MOTION_MASK|GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK,
		   NULL, gdk_cursor_new(GDK_ARROW), GDK_CURRENT_TIME);
  gtk_main();
  return 0;
}
