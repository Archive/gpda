#include "gtkpeninput.h"
#include "gtkpeninputbutton.h"

static void
activate_entry(GtkWidget *w)
{
  g_print("Activate: %s\n", gtk_entry_get_text(GTK_ENTRY(w)));
  gtk_entry_set_text(GTK_ENTRY(w), "");
}

int main(int argc, char *argv[])
{
  GtkWidget *mainwin, *pi, *pib, *vbox, *ent, *bb, *wtmp, *vp;

  gtk_init(&argc, &argv);

  mainwin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize(mainwin, 240, 320);
  gtk_signal_connect(GTK_OBJECT(mainwin), "destroy", gtk_main_quit, NULL);

  vbox = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(mainwin), vbox);

  /* The top button bar */
  bb = gtk_hbutton_box_new();
  gtk_widget_show(bb);
  gtk_box_pack_start(GTK_BOX(vbox), bb, FALSE, FALSE, 5);

  wtmp = gtk_button_new_with_label("Prog 1");
  gtk_widget_show(wtmp);
  gtk_container_add(GTK_CONTAINER(bb), wtmp);

  wtmp = gtk_button_new_with_label("Prog 2");
  gtk_widget_show(wtmp);
  gtk_container_add(GTK_CONTAINER(bb), wtmp);

  pi = gtk_pen_input_new();
  gtk_widget_show(pi);
  gtk_container_add(GTK_CONTAINER(vbox), pi);

  pib = gtk_pen_input_button_new(GTK_PEN_INPUT(pi));
  gtk_widget_show(pib);
  gtk_container_add(GTK_CONTAINER(bb), pib);

  vp = gtk_vpaned_new();
  gtk_widget_show(vp);
  gtk_box_pack_start(GTK_BOX(vbox), vp, FALSE, FALSE, 5);

  wtmp = gtk_calendar_new();
  gtk_widget_show(wtmp);
  gtk_container_add(GTK_CONTAINER(vp), wtmp);

  ent = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(ent), "activate", GTK_SIGNAL_FUNC(activate_entry), NULL);
  gtk_widget_show(ent);
  gtk_container_add(GTK_CONTAINER(vp), ent);
  
  gtk_widget_show(mainwin);
  gtk_main();
  return 0;
}
