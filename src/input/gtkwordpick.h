#ifndef GTK_WORDPICK_H
#define GTK_WORDPICK_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_WORDPICK (gtk_wordpick_get_type())
#define GTK_WORDPICK(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_WORDPICK, GtkWordpick))
#define GTK_WORDPICK_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_WORDPICK, GtkWordpickClass))
#define GTK_IS_WORDPICK(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_WORDPICK))
#define GTK_IS_WORDPICK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_WORDPICK))
#define GTK_WORDPICK_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_WORDPICK, GtkWordpickClass))

typedef struct {
  GtkDrawingArea parent;
  GdkPoint clicked_item;
} GtkWordpick;

typedef struct {
  GtkDrawingAreaClass parent_class;

  PangoGlyphString *gstr;
  PangoFont *font;

  void (* emit_key) (GtkWordpick *w, guint keyval);
  void (* emit_string) (GtkWordpick *w, const char *str);
} GtkWordpickClass;

GType gtk_wordpick_get_type(void);
GtkWidget *gtk_wordpick_new(void);

#endif
