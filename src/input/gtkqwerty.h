#ifndef GTKQWERTY_H
#define GTKQWERTY_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_QWERTY (gtk_qwerty_get_type())
#define GTK_QWERTY(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_QWERTY, GtkQwerty))
#define GTK_QWERTY_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_QWERTY, GtkQwertyClass))
#define GTK_IS_QWERTY(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_QWERTY))
#define GTK_IS_QWERTY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_QWERTY))
#define GTK_QWERTY_GET_CLASS(obj)  (G_CHECK_GET_CLASS ((obj), GTK_TYPE_QWERTY, GtkQwertyClass))

typedef enum {
  GTK_KEY_TOGGLES_CTRL = 1<<0,
  GTK_KEY_TOGGLES_ALT = 1<<1,
  GTK_KEY_TOGGLES_SHIFT = 1<<2,
  GTK_KEY_SHOW_SHIFTED = 1<<3,
  GTK_KEY_SHOW_NORMAL = 1<<4,
  GTK_KEY_HOLD_SHIFT = 1<<5,
  GTK_KEY_TOGGLES_MASK = (GTK_KEY_TOGGLES_CTRL
			  |GTK_KEY_TOGGLES_ALT
			  |GTK_KEY_TOGGLES_SHIFT
			  |GTK_KEY_HOLD_SHIFT),
  GTK_KEY_MODIFIER_STYLE = 1<<6
} GtkKeyDescriptionFlags;

typedef struct {
  const char *cstr;
  guint32 keyval;
  guint8 position;
  guint8 width;
  guint8 row;
  guint8 flags;
} GtkKeyDescription;

extern const GtkKeyDescription gtk_qwerty_keys[];
extern const GtkKeyDescription gtk_qwerty_opti_keys[];

typedef struct _GtkKeyDescriptionInternal GtkKeyDescriptionInternal;

typedef struct {
  GtkDrawingArea parent;
  PangoFont *font;
  PangoFontDescription *fdes;
  GdkPoint press_loc;

  int row_height;
  int num_keys, num_columns, num_rows;
  const GtkKeyDescription *key_info;
  GtkKeyDescriptionInternal *key_int;
  guint8 key_flags;
} GtkQwerty;

typedef struct {
  GtkDrawingAreaClass parent_class;

  void (* emit_key)(GtkQwerty *q, guint keyval);
} GtkQwertyClass;

GType gtk_qwerty_get_type(void);
GtkWidget *gtk_qwerty_new(const GtkKeyDescription *key_info);
void gtk_qwerty_set_key_info(GtkQwerty *q, const GtkKeyDescription *key_info);

#endif
