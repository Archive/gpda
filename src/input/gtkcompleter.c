#include "gtkcompleter.h"
#include <string.h>
#include <gdk/gdkkeysyms.h>
#include <ctype.h>
#include <stdio.h>
#include "lemma.h"

#define DEFAULT_ROW_HEIGHT 20
#define DEFAULT_COL_WIDTH 10
#define SPACE_WIDTH 4
#define SPACE_HEIGHT 1

static void gtk_completer_class_init(GtkCompleterClass *klass);
static void gtk_completer_init(GtkCompleter *q);
static void gtk_completer_finalize(GObject *obj);
static void gtk_completer_size_request(GtkWidget *widget, GtkRequisition *req);
static void gtk_completer_size_allocate(GtkWidget *widget, GtkAllocation *al);
static gboolean gtk_completer_expose (GtkWidget *widget, GdkEventExpose *event);
static gboolean gtk_completer_button_press_event(GtkWidget *widget,
					      GdkEventButton *event);
static gboolean gtk_completer_button_release_event(GtkWidget *widget,
						GdkEventButton *event);
static void gtk_completer_emit_match(GtkWidget *widget,
				     guint keyval,
				     const char *str);

enum {
  EMIT_KEY,
  LAST_SIGNAL
};
static guint completer_signals[LAST_SIGNAL] = { 0 };

GType
gtk_completer_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkCompleterClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_completer_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,           /* class_data */
        sizeof (GtkCompleter),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_completer_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					"GtkCompleter",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

/* Hack! */
static GList default_label6, default_label5, default_label4, default_label3;
static GList default_label2, default_labels;

static GList default_label7 = {"Shift", NULL, &default_label6};
static GList default_label6 = {"Ret", &default_label7, &default_label5};
static GList default_label5 = {"Bksp", &default_label6, &default_label4};
static GList default_label4 = {"Space", &default_label5, &default_label3};
static GList default_label3 = {"KEY", &default_label4, &default_label2};
static GList default_label2 = {"@*!?", &default_label3, &default_labels};
static GList default_labels = {"123", &default_label2, NULL};

static void
gtk_completer_class_init(GtkCompleterClass *klass)
{
  GtkWidgetClass *wclass;
  parent_class = g_type_class_peek_parent(klass);
  G_OBJECT_CLASS(klass)->finalize = gtk_completer_finalize;
  wclass = GTK_WIDGET_CLASS(klass);
  wclass->button_press_event = gtk_completer_button_press_event;
  wclass->button_release_event = gtk_completer_button_release_event;
  wclass->size_request = gtk_completer_size_request;
  wclass->size_allocate = gtk_completer_size_allocate;
  wclass->expose_event = gtk_completer_expose;

  completer_signals[EMIT_KEY] =
    gtk_signal_new("emit_key",
		   GTK_RUN_FIRST,
		   GTK_CLASS_TYPE(wclass),
		   GTK_SIGNAL_OFFSET(GtkCompleterClass, emit_key),
		   g_cclosure_marshal_VOID__UINT,
		   GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
}

static int
my_strncasecmp(const char *s1, const char *s2, size_t n)
{
  size_t i;

  for(i = 0; i < n; i++)
    {
      char c2;
      char c1;

      c2 = tolower(*s2);
      c1 = tolower(*s1);
      s1++;
      s2++;

      if(c1 == '[')
	{
	  const char *ctmp;
	  int retval = -1;

	  for(ctmp = s1; *ctmp && *ctmp != ']'; ctmp++)
	    {
	      c1 = tolower(*ctmp);
	      if(c1 == c2)
		retval = 0;
	    }

	  if(retval)
	    return -1;
	  s1 = ctmp + 1;
	}
      else if(c1 != c2)
	return -1;
    }

  return 0;
}

static void
gtk_completer_init(GtkCompleter *q)
{
  GList *items;

#if 0
  FILE *fh;
  GList *llast;
  char buf[128];

  items = llast = NULL;
  fh = fopen("lemma.out", "r");
  while(fh && fgets(buf, sizeof(buf), fh))
    {
      GList *lnew;
      g_strstrip(buf);
      if(!buf[0])
	continue;

      lnew = g_list_alloc();
      lnew->data = g_strdup(buf);
      lnew->prev = llast;
      if(llast)
	llast->next = lnew;
      if(!items)
	items = lnew;
      llast = lnew;
    }
  if(fh)
    fclose(fh);
#else
  items = (GList *)lemma;
#endif

  q->cmpl = g_completion_new(NULL);
  g_completion_set_compare(q->cmpl, my_strncasecmp);

  if(items)
    g_completion_add_items(q->cmpl, items);
  q->fdes = pango_font_description_new();
  pango_font_description_set_family(q->fdes,"Sans");
  pango_font_description_set_style(q->fdes,PANGO_STYLE_NORMAL);
  pango_font_description_set_variant(q->fdes,PANGO_VARIANT_NORMAL);
  pango_font_description_set_weight(q->fdes,PANGO_WEIGHT_NORMAL);
  pango_font_description_set_stretch(q->fdes,PANGO_STRETCH_NORMAL);
  pango_font_description_set_size(q->fdes,8*PANGO_SCALE);
  gtk_widget_add_events(GTK_WIDGET(q),
			GDK_EXPOSURE_MASK
			|GDK_BUTTON_PRESS_MASK
			|GDK_BUTTON_RELEASE_MASK);
}

static void
gtk_completer_finalize(GObject *obj)
{
  GtkCompleter *c = GTK_COMPLETER(obj);
  if(c->cmpl) {
    g_completion_free(c->cmpl);
    c->cmpl = NULL;
  }

  if(c->fdes) {
    pango_font_description_free(c->fdes);
    c->fdes = NULL;
  }

  parent_class->finalize(obj);
}

static void
gtk_completer_size_allocate(GtkWidget *widget, GtkAllocation *al)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkCompleter *c = GTK_COMPLETER(widget);

  if(wclass->size_allocate)
    wclass->size_allocate(GTK_WIDGET(c), al);
}

static void
gtk_completer_setup_analysis(GtkCompleter *c, PangoAnalysis *anl)
{
  int new_size;

  new_size = 8*PANGO_SCALE;
  if(new_size != pango_font_description_get_size(c->fdes) || !c->font)
    {
      PangoContext *ctxt;

      if(c->font)
	g_object_unref(G_OBJECT(c->font));
      pango_font_description_set_size(c->fdes,new_size);
      ctxt = gtk_widget_get_pango_context(GTK_WIDGET(c));
      c->font = pango_context_load_font(ctxt, c->fdes);
    }

  anl->shape_engine = NULL;
  anl->lang_engine = NULL;
  anl->font = c->font;
  anl->level = 0;
  anl->shape_engine = pango_font_find_shaper(c->font, pango_language_from_string("C"),
					     g_utf8_get_char("X"));
}

static gboolean
gtk_completer_handle_string(GtkWidget *widget,
			    GdkRectangle *area,
			    PangoAnalysis *anl,
			    const char *str,
			    guint *curx, guint *maxy, guint *strwidth,
			    gboolean do_draw,
			    guint maxx)
{
  PangoGlyphString *gstr;
  PangoRectangle prect;
  int wide;
  GtkCompleter *c = (GtkCompleter *)widget;

  gstr = pango_glyph_string_new();
  pango_shape(str, strlen(str), anl, gstr);
  pango_glyph_string_extents(gstr, anl->font, NULL, &prect);
  if(do_draw)
    {
      GtkStateType state;

      if((PANGO_PIXELS(prect.width) + *curx + SPACE_WIDTH * 2) > maxx)
	return TRUE;

      if(str == c->clicked)
	{
	  state = GTK_STATE_ACTIVE;
	  gtk_paint_box(widget->style, widget->window,
			state, GTK_SHADOW_IN, area, widget, NULL,
			*curx, 0, PANGO_PIXELS(prect.width) + SPACE_WIDTH * 2,
			MAX(10, PANGO_PIXELS(prect.height)) + SPACE_HEIGHT*2);
	}
      else
	state = GTK_STATE_NORMAL;

      gdk_draw_glyphs(widget->window, widget->style->text_gc[state],
		      anl->font,
		      *curx + SPACE_WIDTH, -PANGO_PIXELS(prect.y) + SPACE_HEIGHT,
		      gstr);
    }
  wide = PANGO_PIXELS(prect.width) + SPACE_WIDTH * 2;
  *curx += wide + SPACE_WIDTH;
  if(maxy)
    *maxy = MAX(*maxy, PANGO_PIXELS(prect.height) + SPACE_HEIGHT * 2);
  if(strwidth)
    *strwidth = wide;
  pango_glyph_string_free(gstr);

  return FALSE;
}

static void
gtk_completer_size_request(GtkWidget *widget, GtkRequisition *req)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkCompleter *c = GTK_COMPLETER(widget);
  GList *cur;
  PangoAnalysis anl;

  if(wclass->size_request)
    wclass->size_request(widget, req);

  if(c->font)
    {
      PangoFontMetrics *metrics;

      metrics = pango_font_get_metrics(c->font, pango_language_from_string("C"));
      req->height = PANGO_PIXELS(pango_font_metrics_get_ascent(metrics) + pango_font_metrics_get_descent(metrics));
      pango_font_metrics_unref(metrics);
    }
  else
    req->height = 8;

  req->width = 0;
  gtk_completer_setup_analysis(c, &anl);
  for(cur = c->completions?c->completions:&default_labels; cur; cur = cur->next)
    {
      gtk_completer_handle_string(widget, NULL, &anl, cur->data, &req->width,
				  &req->height, NULL, FALSE, 0);
    }
#ifndef ALEX_FIX
  req->height += SPACE_HEIGHT * 2;
#endif
  req->width = MIN(req->width, gdk_screen_width()/2);
}

static void
gtk_completer_paint(GtkWidget *widget, GdkRectangle *area)
{
  PangoAnalysis anl;
  GList *cur;
  int curx;
  int nextwide;

  GtkCompleter *q = GTK_COMPLETER(widget);

  if(!GTK_WIDGET_DRAWABLE(widget))
    return;

  gdk_window_clear_area (widget->window, area->x, area->y,
			 area->width, area->height);

  gtk_completer_setup_analysis(q, &anl);
  for(curx = nextwide = 0, cur = q->completions?q->completions:&default_labels;
      cur && curx < widget->allocation.width
	&& curx < (area->x + area->width);
      cur = cur->next)
    {
      if(gtk_completer_handle_string(widget, area, &anl, cur->data,
				     &curx, NULL, NULL, TRUE,
				     widget->allocation.width))
	break;
    }
}

static gboolean
gtk_completer_expose (GtkWidget *widget, GdkEventExpose *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->expose_event)
    wclass->expose_event(widget, event);

  if(GTK_WIDGET_DRAWABLE(widget))
     gtk_completer_paint(widget, &event->area);

  return TRUE;
}

static void
gtk_completer_handle_button(GtkWidget *widget, GdkEventButton *event)
{
  guint curx, newx;
  GList *l, *cur;
  GtkCompleter *c = GTK_COMPLETER(widget);
  char *evstr = NULL;

  if(event->y < widget->allocation.height)
    {
      PangoAnalysis anl;
      gtk_completer_setup_analysis(c, &anl);
      l = c->completions?c->completions:&default_labels;
      for(curx = newx = 0, cur = l; cur; cur = cur->next, curx = newx)
	{
	  guint wide;
	  gtk_completer_handle_string(widget, NULL, &anl, cur->data,
				      &newx, NULL, &wide, FALSE, 0);
	  if(event->x >= curx && event->x < (curx + wide))
	    {
	      evstr = cur->data;
	      break;
	    }
	}
    }

  if(event->type == GDK_BUTTON_RELEASE)
    {
      if(c->clicked && c->clicked == evstr)
	{
	  if(evstr == default_label7.data)
	    gtk_completer_emit_match(widget, GDK_Shift_L, NULL);
	  else if(evstr == default_label6.data)
	    gtk_completer_emit_match(widget, GDK_Return, NULL);
	  else if(evstr == default_label5.data)
	    gtk_completer_emit_match(widget, GDK_BackSpace, NULL);
	  else if(evstr == default_label4.data)
	    gtk_completer_emit_match(widget, ' ', NULL);
	  else
	    gtk_completer_emit_match(widget, 0, evstr);
	}
      c->clicked = NULL;
      c->buf_len = 0;
      c->buf[0] = '\0';
      c->completions = NULL;
    }
  else
    c->clicked = evstr;

  gtk_widget_queue_draw(widget);
}

static gboolean
gtk_completer_button_press_event(GtkWidget *widget, GdkEventButton *event)
{
  if(event->button == 1)
    gtk_completer_handle_button(widget, event);
  return TRUE;
}

static gboolean
gtk_completer_button_release_event(GtkWidget *widget, GdkEventButton *event)
{
  if(event->button == 1)
    gtk_completer_handle_button(widget, event);
  return TRUE;
}

GtkWidget *
gtk_completer_new(void)
{
  GtkWidget *retval;
  retval = gtk_widget_new(gtk_completer_get_type(), NULL);

  return retval;
}

static void
gtk_completer_post_dinner_cleanup(GtkCompleter *c)
{
  if(c->buf_len)
    c->completions = g_completion_complete(c->cmpl, c->buf, NULL);
  else
    c->completions = NULL;
  c->clicked = NULL;
  gtk_widget_queue_draw(GTK_WIDGET(c));
}

void
gtk_completer_eat_string(GtkCompleter *c, const char *str)
{
  int len = strlen(str);
  if((c->buf_len + len + 1) > sizeof(c->buf))
    g_error("Oversized!");
  memcpy(c->buf + c->buf_len, str, len + 1);
  c->buf_len += len;

  gtk_completer_post_dinner_cleanup(c);
}

void
gtk_completer_eat_key(GtkCompleter *c, guint keyval)
{
  gboolean add_key = FALSE;

  if(keyval < 128 && isalpha(keyval))
    add_key = TRUE;

  switch(keyval)
    {
    case GDK_BackSpace:
      if(c->buf_len > 0)
	c->buf_len--;
      break;
    default:
      if(!add_key)
	c->buf_len = 0;
      break;
    }
  if(add_key && c->buf_len < (sizeof(c->buf) - 1))
    {
      c->buf[c->buf_len] = keyval;
      c->buf_len++;
    }
  c->buf[c->buf_len] = '\0';

  gtk_completer_post_dinner_cleanup(c);
}

static void
gtk_completer_emit_match(GtkWidget *widget,
			 guint keyval,
			 const char *str)
{

  GtkCompleter *c = GTK_COMPLETER(widget);
  if(keyval)
    gtk_signal_emit(GTK_OBJECT(widget), completer_signals[EMIT_KEY], keyval);
  else
    {
      guint aval;
      int i;
      for(i = strlen(c->buf); str[i]; i++)
	{
	  aval = str[i];
	  gtk_signal_emit(GTK_OBJECT(widget), completer_signals[EMIT_KEY], aval);
	}
    }
}
