#ifndef GTK_PEN_INPUT_BUTTON_H
#define GTK_PEN_INPUT_BUTTON_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_PEN_INPUT_BUTTON (gtk_pen_input_button_get_type())
#define GTK_PEN_INPUT_BUTTON(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_PEN_INPUT_BUTTON, GtkPenInputButton))
#define GTK_PEN_INPUT_BUTTON_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_PEN_INPUT_BUTTON, GtkPenInputButtonClass))
#define GTK_IS_PEN_INPUT_BUTTON(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_PEN_INPUT_BUTTON))
#define GTK_IS_PEN_INPUT_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_PEN_INPUT_BUTTON))
#define GTK_PEN_INPUT_BUTTON_GET_CLASS(obj)  (G_CHECK_GET_CLASS ((obj), GTK_TYPE_PEN_INPUT_BUTTON, GtkPenInputButtonClass))

typedef struct {
  GtkToggleButton button;
  guint choose_menu_timer;
  GdkPixbuf *mode_icons[GTK_PEN_INPUT_LAST];
  GtkWidget *mode_menu[GTK_PEN_INPUT_LAST];
  GtkWidget *choose_menu, *mode_img;
  GtkPenInput *pi;
} GtkPenInputButton;

typedef struct {
  GtkToggleButtonClass button_class;
} GtkPenInputButtonClass;

GType gtk_pen_input_button_get_type(void);
GtkWidget *gtk_pen_input_button_new(GtkPenInput *pi);

#endif

