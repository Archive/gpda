/* GPDA - The Gnome PDA
 * Copyright (C) 2002 Xavier Ordoquy
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

//#include "gtkbox.h"
#include "pdapanel.h"

#define _(String) (String)
#define N_(String) (String)
#define textdomain(String) (String)
#define gettext(String) (String)
#define dgettext(Domain,String) (String)
#define dcgettext(Domain,String,Type) (String)
#define bindtextdomain(Domain,Directory) (Domain) 


static void pda_panel_class_init (PdaPanelClass    *klass);
static void pda_panel_init       (PdaPanel         *panel);
static void pda_panel_set_property (GObject         *object,
				    guint            prop_id,
				    const GValue    *value,
				    GParamSpec      *pspec);
static void pda_panel_get_property (GObject         *object,
				    guint            prop_id,
				    GValue          *value,
				    GParamSpec      *pspec);
static void pda_panel_add        (GtkContainer   *container,
				  GtkWidget      *widget);
static void pda_panel_remove     (GtkContainer   *container,
				  GtkWidget      *widget);
static void pda_panel_forall     (GtkContainer   *container,
				  gboolean	include_internals,
				  GtkCallback     callback,
				  gpointer        callback_data);
static void pda_panel_set_child_property (GtkContainer    *container,
					  GtkWidget       *child,
					  guint            property_id,
					  const GValue    *value,
					  GParamSpec      *pspec);
static void pda_panel_get_child_property (GtkContainer    *container,
					  GtkWidget       *child,
					  guint            property_id,
					  GValue          *value,
					  GParamSpec      *pspec);
static GType pda_panel_child_type (GtkContainer   *container);

static void pda_panel_size_request  (GtkWidget      *widget,
				     GtkRequisition *requisition);
static void pda_panel_size_allocate (GtkWidget      *widget,
				     GtkAllocation  *allocation);
static void pda_panel_shift_left(GtkButton *button,
				 gpointer user_data);
static void pda_panel_shift_right(GtkButton *button,
				  gpointer user_data);


static GtkContainerClass *parent_class = NULL;


GType
pda_panel_get_type (void)
{
  static GType panel_type = 0;

  if (!panel_type)
    {
      static const GTypeInfo panel_info =
      {
	sizeof (PdaPanelClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) pda_panel_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	sizeof (PdaPanel),
	1,
	(GInstanceInitFunc) pda_panel_init,
      };
      
      panel_type = g_type_register_static (GTK_TYPE_CONTAINER, "PdaPanel", &panel_info, 0);
    }

  return panel_type;
}

static void
pda_panel_class_init (PdaPanelClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkContainerClass *container_class = GTK_CONTAINER_CLASS (class);
  GtkWidgetClass *widget_class;

  parent_class = g_type_class_peek_parent (class);
  widget_class = (GtkWidgetClass*) class;

  gobject_class->set_property = pda_panel_set_property;
  gobject_class->get_property = pda_panel_get_property;
   
  widget_class->size_request = pda_panel_size_request;
  widget_class->size_allocate = pda_panel_size_allocate;

  container_class->add = pda_panel_add;
  container_class->remove = pda_panel_remove;
  container_class->forall = pda_panel_forall;
  container_class->child_type = pda_panel_child_type;
  container_class->set_child_property = pda_panel_set_child_property;
  container_class->get_child_property = pda_panel_get_child_property;

}

static void
pda_panel_init (PdaPanel *panel)
{
  GTK_WIDGET_SET_FLAGS (panel, GTK_NO_WINDOW);
  gtk_widget_set_redraw_on_allocate (GTK_WIDGET (panel), FALSE);
  
  panel->children = NULL;

  panel->goleft = gtk_button_new();
  panel->goright = gtk_button_new();

  gtk_container_add(GTK_CONTAINER(panel->goleft),
		    gtk_image_new_from_stock(GTK_STOCK_GO_BACK,GTK_ICON_SIZE_SMALL_TOOLBAR));
  gtk_container_add(GTK_CONTAINER(panel->goright),
		    gtk_image_new_from_stock(GTK_STOCK_GO_FORWARD,GTK_ICON_SIZE_SMALL_TOOLBAR));

  gtk_button_set_relief (GTK_BUTTON(panel->goleft),  GTK_RELIEF_NONE);
  gtk_button_set_relief (GTK_BUTTON(panel->goright), GTK_RELIEF_NONE);

  gtk_widget_set_parent (panel->goleft,  GTK_WIDGET (panel));
  gtk_widget_set_parent (panel->goright, GTK_WIDGET (panel));

  g_signal_connect (G_OBJECT (panel->goleft), "clicked",
		    G_CALLBACK (pda_panel_shift_left), panel);
  g_signal_connect (G_OBJECT (panel->goright), "clicked",
		    G_CALLBACK (pda_panel_shift_right), panel);
}

GtkWidget*
pda_panel_new ()
{
  PdaPanel *panel;

  panel = gtk_type_new (pda_panel_get_type ());

  return GTK_WIDGET (panel);
}

static void 
pda_panel_set_property (GObject         *object,
		      guint            prop_id,
		      const GValue    *value,
		      GParamSpec      *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
}

static void pda_panel_get_property (GObject         *object,
				  guint            prop_id,
				  GValue          *value,
				  GParamSpec      *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
}

static GType
pda_panel_child_type	(GtkContainer   *container)
{
  return GTK_TYPE_WIDGET;
}

static void
pda_panel_set_child_property (GtkContainer    *container,
			    GtkWidget       *child,
			    guint            property_id,
			    const GValue    *value,
			    GParamSpec      *pspec)
{
  GTK_CONTAINER_WARN_INVALID_CHILD_PROPERTY_ID (container, property_id, pspec);
}

static void
pda_panel_get_child_property (GtkContainer *container,
			    GtkWidget    *child,
			    guint         property_id,
			    GValue       *value,
			    GParamSpec   *pspec)
{
  GTK_CONTAINER_WARN_INVALID_CHILD_PROPERTY_ID (container, property_id, pspec);
}

void
pda_panel_pack_start (PdaPanel  *panel,
		      GtkWidget *child)
{

  g_return_if_fail (PDA_IS_PANEL (panel));
  g_return_if_fail (GTK_IS_WIDGET (child));
  g_return_if_fail (child->parent == NULL);

  panel->children = g_list_append (panel->children, child);

  gtk_widget_freeze_child_notify (child);

  if( GTK_IS_BUTTON(child) ) {
    gtk_button_set_relief (GTK_BUTTON(child), GTK_RELIEF_NONE);
  }

  gtk_widget_set_parent (child, GTK_WIDGET (panel));
  
  gtk_widget_thaw_child_notify (child);
}

static void
pda_panel_add (GtkContainer *container,
	       GtkWidget    *widget)
{
  pda_panel_pack_start (PDA_PANEL (container), widget);
}

static void
pda_panel_remove (GtkContainer *container,
		  GtkWidget    *widget)
{
  PdaPanel *panel;
  GList *children;

  panel = PDA_PANEL (container);

  children = panel->children;
  while (children)
    {

      if (children->data == widget)
	{
	  gboolean was_visible;

	  was_visible = GTK_WIDGET_VISIBLE (widget);
	  gtk_widget_unparent (widget);

	  panel->children = g_list_remove_link (panel->children, children);
	  g_list_free (children);

	  /* queue resize regardless of GTK_WIDGET_VISIBLE (container),
	   * since that's what is needed by toplevels.
	   */
	  if (was_visible)
	    gtk_widget_queue_resize (GTK_WIDGET (container));

	  break;
	}

      children = children->next;
    }
}

static void
pda_panel_forall (GtkContainer *container,
		gboolean      include_internals,
		GtkCallback   callback,
		gpointer      callback_data)
{
  PdaPanel *panel;
  GList *children;

  g_return_if_fail (callback != NULL);

  panel = PDA_PANEL (container);

  if( panel->goleft != NULL )
    (* callback) (panel->goleft, callback_data);
  if( panel->goright != NULL )
    (* callback) (panel->goright, callback_data);

  children = panel->children;
  while (children)
    {
      (* callback) (children->data, callback_data);
      children = children->next;
    }
}


static void
pda_panel_size_request (GtkWidget      *widget,
		       GtkRequisition *requisition)
{
  GtkRequisition child_requisition;
  PdaPanel *panel;
  GList *children;
  gint nvis_children;

  panel = PDA_PANEL (widget);
  requisition->width = 0;
  requisition->height = 0;
  nvis_children = 0;

  gtk_widget_size_request (panel->goleft,  &child_requisition);
  gtk_widget_size_request (panel->goright, &child_requisition);

  children = panel->children;
  while (children) {

    if (GTK_WIDGET_VISIBLE (children->data)) {

      gtk_widget_size_request (children->data, &child_requisition);

      requisition->width += child_requisition.width;
      requisition->height = MAX (requisition->height, child_requisition.height);

      nvis_children += 1;
    }

    children = children->next;
  }

  if (nvis_children > 0) {
    requisition->width += (nvis_children - 1);
  }

  requisition->width += GTK_CONTAINER (panel)->border_width * 2;
  requisition->height += GTK_CONTAINER (panel)->border_width * 2;
}

static void
pda_panel_size_allocate (GtkWidget     *widget,
			GtkAllocation *allocation)
{
  PdaPanel *panel;
  GList *children;
  GtkAllocation child_allocation;
  gint nvis_children;
  gint x, x_max;
  GtkTextDirection direction;
  GtkRequisition child_requisition;

  panel = PDA_PANEL (widget);
  widget->allocation = *allocation;

  direction = gtk_widget_get_direction (widget);
  
  nvis_children = 0;
  children = panel->children;

  if (children != NULL) {

    x = allocation->x + GTK_CONTAINER (panel)->border_width;
    x_max = allocation->x + allocation->width - GTK_CONTAINER (panel)->border_width;
    child_allocation.y = allocation->y + GTK_CONTAINER (panel)->border_width;
    child_allocation.height = MAX (1, (gint) allocation->height - (gint) GTK_CONTAINER (panel)->border_width * 2);

    if( allocation->width < widget->requisition.width ) {

      gtk_widget_get_child_requisition (panel->goleft, &child_requisition);

      child_allocation.x = x;
      child_allocation.width = child_requisition.width;
      x += child_requisition.width;

      gtk_widget_size_allocate (panel->goleft, &child_allocation);

      gtk_widget_get_child_requisition (panel->goright, &child_requisition);
      x_max -= child_requisition.width;
      gtk_widget_show(panel->goleft);
    } else {
      gtk_widget_hide(panel->goleft);
    }

    children = panel->children;
    while (children) {

      if (GTK_WIDGET_VISIBLE (children->data)) {

	gtk_widget_get_child_requisition (children->data, &child_requisition);

	if( x+child_requisition.width > x_max ) {

	  GtkAllocation alloc = {-1,-1,1,1};
	  gtk_widget_size_allocate (children->data, &alloc);

	} else {

	  child_allocation.width = child_requisition.width;
	  child_allocation.x = x;

	  if (direction == GTK_TEXT_DIR_RTL)
	    child_allocation.x = allocation->x + allocation->width - (child_allocation.x - allocation->x) - child_allocation.width;

	  gtk_widget_size_allocate (children->data, &child_allocation);

	  x += child_allocation.width;
	}
      }
      children = children->next;
    }

    if( allocation->width < widget->requisition.width ) {

      gtk_widget_get_child_requisition (panel->goright, &child_requisition);

      child_allocation.x = x;
      child_allocation.width = child_requisition.width;

      gtk_widget_size_allocate (panel->goright, &child_allocation);
      gtk_widget_show(panel->goright);
    } else {
      gtk_widget_hide(panel->goright);
    }
  }
}

static void
pda_panel_shift_left(GtkButton *button,
		     gpointer user_data)
{
  PdaPanel *panel;
  GList *element;

  panel = PDA_PANEL(user_data);
  element = g_list_first(panel->children);
  panel->children = g_list_remove_link(panel->children,element);
  panel->children = g_list_concat(panel->children,element);
}

static void
pda_panel_shift_right(GtkButton *button,
		      gpointer user_data)
{
  PdaPanel *panel;
  GList *element;

  panel = PDA_PANEL(user_data);
  element = g_list_last(panel->children);
  panel->children = g_list_remove_link(panel->children,element);
  panel->children = g_list_concat(element,panel->children);
}

void
pda_panel_clean(PdaPanel *panel)
{
  //gtk_container_foreach(GTK_CONTAINER(panel),pda_panel_clean_callback,panel);
  GList *childs;

  childs = gtk_container_get_children(GTK_CONTAINER(panel));
  childs = g_list_last(childs);
  while(childs != NULL) {
    GList *previous = childs->prev;
    gtk_container_remove(GTK_CONTAINER(panel),GTK_WIDGET(childs->data));
    childs = previous;
  }
}
