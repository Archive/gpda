#include <gtk/gtkmain.h>
#include <gtk/gtkwindow.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkcontainer.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkimage.h>
#include <gtk/gtksignal.h>
#include "pdaapplication.h"
#include "pdaappcontainer.h"
#include "pdaui.h"

int
main (int argc, char *argv[])
{
  gtk_init (&argc, &argv);

  pda_application_factory_register_dir ("./test/.libs");
  pda_application_factory_register_dir ("./gnomine/.libs");
  pda_application_factory_register_dir ("./applications/.libs");
  /*  pda_application_factory_register_dir (PKGLIBDIR); */

  pda_ui_create ();
  
  gtk_main ();
  return 0;
}
