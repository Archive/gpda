/* GPDA - The Gnome PDA
 * Copyright (C) 2001 Red Hat, Inc.
 * Copyright (C) 2002 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * Authors: Alexander Larsson <alexl@redhat.com>
 */

#include "pdaui.h"
#include "pdapanel.h"
#include "pdaappcontainer.h"
#include "pdaclock.h"
#include "pdacommands.h"
#include "input/gtkpeninput.h"
#include "input/gtkpeninputbutton.h"
#include <gtk/gtkmenubar.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkvseparator.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtksignal.h>

#include "pixbufs.h"

GtkWindow *pda_top_window;
PdaAppContainer *pda_app_container;
GtkPenInput *pda_virtual_keyboard;
GtkPenInputButton *pda_virtual_keyboard_control;
GtkWidget *menu;
GtkWidget *panel;

static GtkMenuItem *running_apps_menuitem;
static PdaPanel    *scroller;

gboolean
menu_select_vkb_mode (GtkMenuItem *item,
		      gpointer data)
{
  GtkPenInputMode mode = GPOINTER_TO_INT (data);
  gtk_pen_input_set_mode (pda_virtual_keyboard, mode);
  gtk_widget_show (GTK_WIDGET (pda_virtual_keyboard));
  return TRUE;
}

gboolean
menu_select_vkb_visible (GtkMenuItem *item)
{
  gtk_widget_hide(GTK_WIDGET(pda_virtual_keyboard));

  return TRUE;
}

gboolean
menu_select_vkb_completer (GtkMenuItem *item)
{
  /*
  gboolean completer;
  completer = gtk_pen_input_completer_enabled (pda_virtual_keyboard);
  gtek_pen_input_completer_enable (pda_virtual_keyboard, !completer);
  */
  return TRUE;
}


gboolean
menu_start_app (GtkMenuItem *item,
		PdaApplicationFactory *factory)
{
  pda_start_app (factory);
  return TRUE;
}

gboolean
menu_switch_to_app (GtkMenuItem *item,
		    PdaApplication *app)
{
  pda_app_container_switch_to_app (PDA_APP_CONTAINER (pda_app_container), app);
  
  return TRUE;
}

gboolean
menu_quit_app (GtkMenuItem *item)
{
  PdaApplication *app;
  app = pda_app_container_current (pda_app_container);
  if (app)
    pda_quit_app (app);
  return TRUE;
}

GtkWidget *
create_app_menu_item (PdaApplicationFactory *factory)
{
  GtkWidget *item;
  GdkPixbuf *pixbuf;
  GtkWidget *image;
  
  pixbuf = pda_application_factory_get_small_icon (factory);
  image = gtk_image_new_from_pixbuf (pixbuf);
  
  item = gtk_image_menu_item_new_with_label (factory->name);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item),
				 image);
  g_object_unref (G_OBJECT (pixbuf));
  
  return item;
}

GtkWidget *
create_app_button (PdaApplicationFactory *factory)
{
  GtkWidget *item;
  GdkPixbuf *pixbuf;
  GtkWidget *image;
  
  pixbuf = pda_application_factory_get_small_icon (factory);
  image = gtk_image_new_from_pixbuf (pixbuf);

  item = gtk_button_new();
  gtk_container_add(GTK_CONTAINER(item),image);
  gtk_widget_show(image);

  g_object_unref (G_OBJECT (pixbuf));
  
  return item;
}

GtkWidget *
create_switch_to_app_submenu ()
{
  GList *list;
  GtkWidget *menu;
  GtkWidget *menuitem;
  GtkWidget *button;

  menu = gtk_menu_new ();

  list = pda_applications_running;
  while (list)
    {
      PdaApplication *app = list->data;

      menuitem = create_app_menu_item (app->factory);

      gtk_widget_show (menuitem);
      
      gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
      
      gtk_signal_connect (GTK_OBJECT (menuitem),
			  "activate",
			  (GtkSignalFunc) menu_switch_to_app,
			  app);

      button = create_app_button(app->factory);
      gtk_widget_show(button);
      pda_panel_pack_start(scroller, button);

      gtk_signal_connect (GTK_OBJECT (button),
			  "clicked",
			  (GtkSignalFunc) menu_switch_to_app,
			  app);

      list = g_list_next (list);
    }
  
  return menu;
}

GtkWidget *
create_app_submenu (GQuark category)
{
  GList *list;
  GtkWidget *menu;
  GtkWidget *menuitem;

  menu = gtk_menu_new ();
  
  list = pda_application_factories;
  while (list)
    {
      PdaApplicationFactory *factory = (PdaApplicationFactory *)list->data;

      if (factory->app_category == category)
	{

	  menuitem = create_app_menu_item (factory);
	  
	  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

	  gtk_signal_connect (GTK_OBJECT (menuitem),
			      "activate",
			      (GtkSignalFunc) menu_start_app,
			      factory);
	}

      list = g_list_next (list);
    }
  
  return menu;
}

void
pda_ui_update_running_apps (void)
{
  GtkWidget *submenu;
  pda_panel_clean(scroller);
  submenu = create_switch_to_app_submenu ();
  gtk_widget_show_all (submenu);
  gtk_menu_item_set_submenu (running_apps_menuitem, submenu);
  /* Do we leak the previous menu? */
}

GtkWidget *
create_menu (void)
{
  GQuark category;
  GtkWidget *submenu;
  GtkWidget *menu;
  GtkWidget *menuitem;
  const char *category_name;
  int i;

  menu = gtk_menu_new ();

  for (i = 0; i < pda_n_app_categories; i++)
    {
      category = pda_app_categories[i];

      category_name = g_quark_to_string (category);
      menuitem = gtk_menu_item_new_with_label (category_name);
      gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

      submenu = create_app_submenu (category);
      gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), submenu);
    }

  /* Separator */
  menuitem = gtk_menu_item_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);

  menuitem = gtk_menu_item_new_with_label ("Switch To");
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
  running_apps_menuitem = GTK_MENU_ITEM (menuitem);

  menuitem = gtk_menu_item_new_with_label ("Quit");
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
  gtk_signal_connect (GTK_OBJECT (menuitem),
		      "activate",
		      (GtkSignalFunc) menu_quit_app,
		      NULL);

  gtk_widget_show_all (menu);
  return menu;
}

void
menu_position(GtkMenu  *menu,
	      gint     *x,
	      gint     *y,
	      gboolean *push_in,
	      gpointer  data)
{
  GtkRequisition req;
  GtkWidget *hbox = GTK_WIDGET(data);

  gdk_window_get_origin (hbox->window, x, y);
  if(GTK_WIDGET_NO_WINDOW(hbox)) {
    *x += GTK_WIDGET(hbox)->allocation.x;
    *y += GTK_WIDGET(hbox)->allocation.y;
  }

  gtk_widget_size_request(GTK_WIDGET(menu),&req);
  *y -= req.height;
  *push_in = TRUE;
}

void
menu_button_pressed(GtkButton *menu_button, gpointer user_data)
{
  GdkEventButton *bevent = (GdkEventButton*)gtk_get_current_event();

  gtk_menu_popup(GTK_MENU(menu),
		 NULL,
		 NULL,
		 menu_position,
		 user_data,
		 bevent->button,
		 bevent->time);
  gdk_event_free((GdkEvent *)bevent);
}

void
pda_ui_create (void)
{
  GtkWidget *menu_button;
  GtkWidget *window;
  GtkWidget *bottom;
  GtkWidget *vbox;
  GtkWidget *app_container;
  GtkWidget *clock;
  GtkWidget *image;
  GdkPixbuf *pixbuf;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect(GTK_OBJECT(window), "delete_event", GTK_SIGNAL_FUNC(gtk_exit),
		     GINT_TO_POINTER(0));
  gtk_widget_set_usize (window, 240, 320);
  gtk_window_set_policy (GTK_WINDOW(window), FALSE, FALSE, FALSE);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  bottom = gtk_hbox_new (FALSE, 0);

  app_container = pda_app_container_new ();
  pda_app_container = PDA_APP_CONTAINER (app_container);
  gtk_box_pack_start (GTK_BOX (vbox),
		      app_container,
		      TRUE,
		      TRUE,
		      0);

  gtk_box_pack_end (GTK_BOX (vbox),
		    bottom,
		    FALSE,
		    FALSE,
		    0);

  gtk_widget_show_all (vbox);

  pixbuf = gdk_pixbuf_new_from_inline (sizeof (gnome_foot_small), gnome_foot_small, TRUE, NULL);
  image = gtk_image_new_from_pixbuf (pixbuf);

  menu = create_menu ();

  menu_button = gtk_button_new();
  gtk_button_set_relief(GTK_BUTTON(menu_button),GTK_RELIEF_NONE);
  gtk_container_add(GTK_CONTAINER(menu_button),
		    image);
  gtk_box_pack_start (GTK_BOX (bottom),
		      menu_button,
		      FALSE,
		      FALSE,
		      0);
  gtk_widget_show_all (menu_button);

  g_signal_connect (G_OBJECT (menu_button), "clicked",
		    G_CALLBACK (menu_button_pressed), bottom);

  scroller = PDA_PANEL(pda_panel_new());
  //pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_1_"));
  /*
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_2_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_3_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_4_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_5_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_6_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_7_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_8_"), TRUE, TRUE, 0);
  pda_panel_pack_start (PDA_PANEL (scroller), gtk_button_new_with_label("_9_"), TRUE, TRUE, 0);
  */

  gtk_box_pack_start (GTK_BOX (bottom), GTK_WIDGET(scroller), TRUE, TRUE, 0);
  gtk_widget_show_all (GTK_WIDGET(scroller));

  clock = pda_clock_new (TRUE);
  gtk_box_pack_end (GTK_BOX (bottom), clock, FALSE, FALSE, 0);
  gtk_widget_show (clock);

  /*
  //Virtual keyboard menu
  vkb = gtk_pen_input_new ();
  gtk_box_pack_end (GTK_BOX (vbox), vkb,
		    FALSE, FALSE, 0);
  pda_virtual_keyboard = GTK_PEN_INPUT (vkb);
  gtk_widget_show (vkb);

  pda_virtual_keyboard_control =
    GTK_PEN_INPUT_BUTTON (gtk_pen_input_button_new (GTK_PEN_INPUT (vkb)));

  gtk_box_pack_end (GTK_BOX (panel),
		    GTK_WIDGET(pda_virtual_keyboard_control),
		    FALSE,
		    FALSE,
		    0);
  gtk_widget_show (GTK_WIDGET(pda_virtual_keyboard_control));
  */

  pda_ui_update_running_apps ();
  gtk_widget_show(window);
}
