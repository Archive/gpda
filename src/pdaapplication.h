/* GPDA - The Gnome PDA
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * Authors: Alexander Larsson <alexl@redhat.com>
 */

#ifndef __PDA_APPLICATION_H__
#define __PDA_APPLICATION_H__

#include <glib/gquark.h>
#include <pdasession.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib-object.h>
#include <gtk/gtkwidget.h>

#define	PDA_TYPE_APPLICATION	          (pda_application_get_type ())
#define PDA_APPLICATION(object)		  (G_TYPE_CHECK_INSTANCE_CAST ((object), PDA_TYPE_APPLICATION, PdaApplication))
#define PDA_APPLICATION_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), PDA_TYPE_APPLICATION, PdaApplicationClass))
#define PDA_IS_APPLICATION(object)	  (G_TYPE_CHECK_INSTANCE_TYPE ((object), PDA_TYPE_APPLICATION))
#define PDA_IS_APPLICATION_CLASS(klass)	  (G_TYPE_CHECK_CLASS_TYPE ((klass), PDA_TYPE_APPLICATION))
#define	PDA_APPLICATION_GET_CLASS(object) (G_TYPE_INSTANCE_GET_CLASS ((object), PDA_TYPE_APPLICATION, PdaApplicationClass))

typedef struct _PdaApplication PdaApplication;
typedef struct _PdaApplicationClass PdaApplicationClass;
typedef struct _PdaApplicationFactory PdaApplicationFactory;

struct _PdaApplication
{
  GObject parent_instance;
  
  PdaApplicationFactory *factory;
  GtkWidget *toplevel_widget;

  gpointer app_data;
};

struct _PdaApplicationClass
{
  GObjectClass parent_class;

  void (*switch_to)    (PdaApplication *app);
  void (*switch_from)  (PdaApplication *app);
  void (*quit)         (PdaApplication *app);
  void (*save_session) (PdaApplication *app,
			PdaSessionObject *session_obj);
};

struct _PdaApplicationFactory
{
  gchar *name;
  GQuark app_category;
  gboolean singleton;

  GdkPixbuf *(*get_small_icon) (PdaApplicationFactory *factory);
  GdkPixbuf *(*get_large_icon) (PdaApplicationFactory *factory);

  PdaApplication *(*start)     (PdaApplicationFactory *factory,
				PdaSessionObject      *session_obj);

  /* Filled in at runtime: */
  PdaApplication *singleton_instance;
};

extern GList *pda_applications_running;
extern GList *pda_application_factories;
extern GQuark *pda_app_categories;
extern gint pda_n_app_categories;

GType pda_application_get_type (void) G_GNUC_CONST;

PdaApplication *pda_application_new (PdaApplicationFactory *factory);

gchar *    pda_application_get_name           (PdaApplication   *app);
GQuark     pda_application_get_category       (PdaApplication   *app);
GdkPixbuf *pda_application_get_small_icon     (PdaApplication   *app);
GdkPixbuf *pda_application_get_large_icon     (PdaApplication   *app);

void       pda_application_quit               (PdaApplication   *app);
void       pda_application_switch_to          (PdaApplication   *app);
void       pda_application_switch_from        (PdaApplication   *app);

void       pda_application_save_session       (PdaApplication   *app,
					       PdaSessionObject *session_obj);

void       pda_application_register_running   (PdaApplication   *app);
void       pda_application_unregister_running (PdaApplication   *app);

typedef void (*pda_register_application_module_func) (void);

GdkPixbuf *     pda_application_factory_get_small_icon (PdaApplicationFactory *factory);
GdkPixbuf *     pda_application_factory_get_large_icon (PdaApplicationFactory *factory);
PdaApplication *pda_application_factory_start          (PdaApplicationFactory *factory,
							PdaSessionObject      *session_obj,
							gboolean              *reactivated);
void            pda_application_factory_register_dir   (const gchar           *directory);

/* Called by application factories while loading */
gboolean        pda_application_factory_register       (PdaApplicationFactory *factory);
GQuark          pda_app_category_register              (const gchar           *category_name);


#endif /* __PDA_APPLICATION_H__ */

