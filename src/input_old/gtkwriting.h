#ifndef GTKWRITING_H
#define GTKWRITING_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_WRITING_INPUT (gtk_writing_input_get_type())
#define GTK_WRITING_INPUT(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_WRITING_INPUT, GtkWritingInput))
#define GTK_WRITING_INPUT_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_WRITING_INPUT, GtkWritingInputClass))
#define GTK_IS_WRITING_INPUT(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_WRITING_INPUT))
#define GTK_IS_WRITING_INPUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_WRITING_INPUT))
#define GTK_WRITING_INPUT_GET_CLASS(obj)  (G_CHECK_GET_CLASS ((obj), GTK_TYPE_WRITING_INPUT, GtkWritingInputClass))

typedef struct {
  GtkDrawingArea button;
  gboolean btn_down : 1;
  GdkPoint prev_loc;
  GdkGC *drawing_gc;
} GtkWritingInput;

typedef struct {
  GtkDrawingAreaClass button_class;
  void (* emit_key) (GtkWritingInput *wi, guint keyval);
} GtkWritingInputClass;

GType gtk_writing_input_get_type(void);
GtkWidget *gtk_writing_input_new(void);

#endif
