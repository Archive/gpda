#ifndef GTK_UNIPICK_H
#define GTK_UNIPICK_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_UNIPICK (gtk_unipick_get_type())
#define GTK_UNIPICK(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_UNIPICK, GtkUnipick))
#define GTK_UNIPICK_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_UNIPICK, GtkUnipickClass))
#define GTK_IS_UNIPICK(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_UNIPICK))
#define GTK_IS_UNIPICK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_UNIPICK))
#define GTK_UNIPICK_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_UNIPICK, GtkUnipickClass))

typedef struct {
  GtkDrawingArea parent;
  GdkPoint clicked_item;
  int num_columns, num_rows;
} GtkUnipick;

typedef struct {
  GtkDrawingAreaClass parent_class;

  PangoFont *font;

  void (* emit_key) (GtkUnipick *u, guint keyval);
} GtkUnipickClass;

GType gtk_unipick_get_type(void);
GtkWidget *gtk_unipick_new(void);

#endif
