#include "gtkpeninput.h"
#include "gtkqwerty.h"
#include "gtkcompleter.h"
#include "gtkunipick.h"
#include "gtkwriting.h"

/* #define USE_WORDPICK 1 */

#ifdef USE_WORDPICK
#include "gtkwordpick.h"
#endif

static void gtk_pen_input_class_init(GtkPenInputClass *klass);
static void gtk_pen_input_init(GtkPenInput *pi);
static void gtk_pen_input_emit_string(GtkObject *obj, const char *str, GtkPenInput *pi);
static void gtk_pen_input_emit_key(GtkObject *obj, guint keyval, GtkPenInput *pi);
static void gtk_pen_input_emit_key_real(GtkObject *obj, guint keyval, GtkPenInput *pi);

GType
gtk_pen_input_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkPenInputClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_pen_input_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkPenInput),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_pen_input_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_VBOX,
					"GtkPenInput",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

static void
gtk_pen_input_class_init(GtkPenInputClass *klass)
{
  parent_class = g_type_class_peek_parent(klass);
}

static void
gtk_pen_input_init(GtkPenInput *pi)
{
  pi->mode = GTK_PEN_INPUT_LAST;
  pi->completer_enabled = TRUE;
}

void
gtk_pen_input_set_mode(GtkPenInput *pi, GtkPenInputMode mode)
{
  GtkWidget *w, *wtmp;

  if(mode == pi->mode)
    return;

  if(pi->mode != GTK_PEN_INPUT_LAST)
    {
      if(pi->modes[pi->mode])
	gtk_widget_hide(pi->modes[pi->mode]);
    }

  pi->mode = mode;

  w = pi->modes[mode];
  if(!w)
    {
      wtmp = NULL;
      switch(mode)
	{
	case GTK_PEN_INPUT_HANDWRITING:
	  w = wtmp = gtk_writing_input_new();
	  break;
#ifdef USE_WORDPICK
	case GTK_PEN_INPUT_PICK:
	  w = wtmp = gtk_wordpick_new();
	  gtk_signal_connect(GTK_OBJECT(wtmp), "emit_string",
			     gtk_pen_input_emit_string, pi);
	  break;
#endif
	case GTK_PEN_INPUT_UNICODE:
	  {
	    w = gtk_scrolled_window_new(NULL, NULL);
	    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(w),
					   GTK_POLICY_NEVER,
					   GTK_POLICY_ALWAYS);
	    wtmp = gtk_unipick_new();
	    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(w), wtmp);
	    gtk_widget_show(wtmp);
	  }
	  break;
	case GTK_PEN_INPUT_QWERTY:
	  w = wtmp = gtk_qwerty_new(NULL);
	  break;
	case GTK_PEN_INPUT_OPTI:
	  w = wtmp = gtk_qwerty_new(gtk_qwerty_opti_keys);
	  break;
	default:
	  g_assert_not_reached();
	  break;
	}

      pi->modes[mode] = w;
      if(wtmp && w)
	{
	  gtk_signal_connect(GTK_OBJECT(wtmp), "emit_key",
			     gtk_pen_input_emit_key, pi);
	  gtk_box_pack_start(GTK_BOX(pi), w, FALSE, FALSE, 0);
	}
    }
  
  if (w)
    gtk_widget_show_all (w);
}

static void
gtk_pen_input_emit_string(GtkObject *obj, const char *str, GtkPenInput *pi)
{
  gtk_completer_eat_string(GTK_COMPLETER(pi->completer), str);
}

static void
gtk_pen_input_emit_key(GtkObject *obj, guint keyval, GtkPenInput *pi)
{
  gtk_completer_eat_key(GTK_COMPLETER(pi->completer), keyval);
  gtk_pen_input_emit_key_real(obj, keyval, pi);
}

static void
gtk_pen_input_emit_key_real(GtkObject *obj, guint keyval, GtkPenInput *pi)
{
  GdkEvent event;
  GtkWidget *topw;

  /* Send it on to the app */
  topw = gtk_widget_get_toplevel((GtkWidget *)pi);
  event.key.type = GDK_KEY_PRESS;
  event.key.window = topw->window;
  event.key.send_event = TRUE;
  event.key.time = GDK_CURRENT_TIME;
  event.key.state = 0;
  event.key.keyval = keyval;
  event.key.length = 0;
  event.key.string = NULL;
  gdk_event_put(&event);

  event.key.type = GDK_KEY_RELEASE;
  gdk_event_put(&event);
}

GtkWidget *
gtk_pen_input_new()
{
  GtkPenInput *pi;

  pi = gtk_type_new (gtk_pen_input_get_type());

  pi->completer = gtk_completer_new();
  gtk_signal_connect(GTK_OBJECT(pi->completer), "emit_key",
		     gtk_pen_input_emit_key_real, pi);
  gtk_box_pack_start(GTK_BOX(pi), pi->completer, FALSE, FALSE, 0);
  gtk_widget_show(pi->completer);
  
  if (pi->mode == GTK_PEN_INPUT_LAST)
    gtk_pen_input_set_mode(pi, GTK_PEN_INPUT_OPTI);

  return GTK_WIDGET (pi);
}

gboolean
gtk_pen_input_completer_enabled (GtkPenInput *pi)
{
  return pi->completer_enabled;
}

void
gtk_pen_input_completer_enable (GtkPenInput *pi, gboolean enable)
{
  if (enable)
    gtk_widget_show (pi->completer);
  else 
    gtk_widget_hide (pi->completer);
  gtk_widget_queue_resize (GTK_WIDGET (pi));
  pi->completer_enabled = enable;
}
