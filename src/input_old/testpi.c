#include "gtkpeninput.h"

static void
activate_entry(GtkWidget *w)
{
  g_print("Activate: %s\n", gtk_entry_get_text(GTK_ENTRY(w)));
  gtk_entry_set_text(GTK_ENTRY(w), "");
}

int main(int argc, char *argv[])
{
  GtkWidget *mainwin, *pi, *vbox, *vb2, *ent;
  gtk_init(&argc, &argv);
  mainwin = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect(GTK_OBJECT(mainwin), "destroy", gtk_main_quit, NULL);
  vbox = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(mainwin), vbox);
  vb2 = gtk_vbox_new(FALSE, 5);
  gtk_widget_show(vb2);
  gtk_container_add(GTK_CONTAINER(vbox), vb2);
  pi = gtk_pen_input_new(vb2);
  gtk_widget_show(pi);
  gtk_box_pack_end(GTK_BOX(vbox), pi, FALSE, FALSE, 0);
  ent = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(ent), "activate", activate_entry, NULL);
  gtk_widget_show(ent);
  gtk_box_pack_start(GTK_BOX(vbox), ent, FALSE, FALSE, 5);
  
  gtk_widget_show(mainwin);
  gtk_main();
  return 0;
}
