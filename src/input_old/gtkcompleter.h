#ifndef GTKCOMPLETER_H
#define GTKCOMPLETER_H 1

#include <gtk/gtk.h>

#define GTK_TYPE_COMPLETER (gtk_completer_get_type())
#define GTK_COMPLETER(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_COMPLETER, GtkCompleter))
#define GTK_COMPLETER_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_COMPLETER, GtkCompleterClass))
#define GTK_IS_COMPLETER(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_COMPLETER))
#define GTK_IS_COMPLETER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_COMPLETER))
#define GTK_COMPLETER_GET_CLASS(obj)  (G_CHECK_GET_CLASS ((obj), GTK_TYPE_COMPLETER, GtkCompleterClass))

typedef struct {
  GtkDrawingArea parent;
  PangoFont *font;
  PangoFontDescription fdes;
  GdkPoint press_loc;

  GCompletion *cmpl;
  char buf[64];
  int buf_len;
  GString *tmpstr;
  GList *completions;
  char *clicked;
} GtkCompleter;

typedef struct {
  GtkDrawingAreaClass parent_class;

  void (* emit_key)(GtkCompleter *c, guint keyval);
} GtkCompleterClass;

GType gtk_completer_get_type(void);
GtkWidget *gtk_completer_new(void);
void gtk_completer_eat_key(GtkCompleter *c, guint keyval);
void gtk_completer_eat_string(GtkCompleter *c, const char *str);

#endif
