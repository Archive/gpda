#include "gtkunipick.h"
#include <string.h>

#define FONT_SIZE 12
#define SPACE_SIZE 8
#define BOX_SIZE (FONT_SIZE + SPACE_SIZE*2)
#define MARGIN_SIZE SPACE_SIZE*2
#define NUM_CHARS 65536

static void gtk_unipick_class_init(GtkUnipickClass *klass);
static void gtk_unipick_init(GtkUnipick *q);
static void gtk_unipick_destroy(GObject *obj);
static void gtk_unipick_size_request(GtkWidget *widget, GtkRequisition *req);
static void gtk_unipick_size_allocate(GtkWidget *widget, GtkAllocation *al);
static gboolean gtk_unipick_expose (GtkWidget *widget, GdkEventExpose *event);
static gboolean gtk_unipick_button_press_event(GtkWidget *widget, GdkEventButton *event);
static gboolean gtk_unipick_button_release_event(GtkWidget *widget, GdkEventButton *event);
static int row_to_y(GtkUnipick *q, int row);
static int col_to_x(GtkUnipick *q, int col);
static int y_to_row(GtkUnipick *q, int y);
static int x_to_col(GtkUnipick *q, int x);

GType
gtk_unipick_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkUnipickClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_unipick_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkUnipick),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_unipick_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					"GtkUnipick",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

enum {
  EMIT_KEY,
  LAST_SIGNAL
};
static guint unipick_signals[LAST_SIGNAL] = { 0 };

static void
gtk_unipick_class_init(GtkUnipickClass *klass)
{
  GtkWidgetClass *wclass;
  parent_class = g_type_class_peek_parent(klass);
  G_OBJECT_CLASS(klass)->shutdown = gtk_unipick_destroy;
  wclass = GTK_WIDGET_CLASS(klass);
  wclass->button_press_event = gtk_unipick_button_press_event;
  wclass->button_release_event = gtk_unipick_button_release_event;
  wclass->size_request = gtk_unipick_size_request;
  wclass->size_allocate = gtk_unipick_size_allocate;
  wclass->expose_event = gtk_unipick_expose;

  unipick_signals[EMIT_KEY] =
    gtk_signal_new("emit_key",
		   GTK_RUN_FIRST,
		   GTK_CLASS_TYPE(wclass),
		   GTK_SIGNAL_OFFSET(GtkUnipickClass, emit_key),
		   gtk_marshal_VOID__UINT,
		   GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
}

static void
gtk_unipick_init(GtkUnipick *u)
{
  GtkUnipickClass *klass;

  klass = GTK_UNIPICK_GET_CLASS(u);
  u->num_columns = 8;
  u->num_rows = NUM_CHARS/u->num_columns;
  u->clicked_item.x = 99;

  if(!klass->font)
    {
      PangoContext *ctxt;
      PangoFontDescription fdes;

      fdes.family_name = "Sans";
      fdes.style = PANGO_STYLE_NORMAL;
      fdes.variant = PANGO_VARIANT_NORMAL;
      fdes.weight = PANGO_WEIGHT_NORMAL;
      fdes.stretch = PANGO_STRETCH_NORMAL;
      fdes.size = FONT_SIZE;

      ctxt = gtk_widget_get_pango_context(GTK_WIDGET(u));
      klass->font = pango_context_load_font(ctxt, &fdes);
    }

  gtk_widget_add_events(GTK_WIDGET(u),
			GDK_EXPOSURE_MASK
			|GDK_BUTTON_PRESS_MASK
			|GDK_BUTTON_RELEASE_MASK);
}

static void
gtk_unipick_destroy(GObject *obj)
{
}

static void
gtk_unipick_size_request(GtkWidget *widget, GtkRequisition *req)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkUnipick *u = (GtkUnipick *)widget;
  int width, height;

  if(wclass->size_request)
    wclass->size_request(widget, req);

  width = u->num_columns * (FONT_SIZE + SPACE_SIZE*2) + SPACE_SIZE;
  height = u->num_rows * (FONT_SIZE + SPACE_SIZE*2) + SPACE_SIZE;

  req->width = MAX(req->width, width);
  req->height = MAX(req->height, height);
}

static void
gtk_unipick_size_allocate(GtkWidget *widget, GtkAllocation *al)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkUnipick *u = (GtkUnipick *)widget;
  int new_num_columns;

  if(wclass->size_allocate)
    wclass->size_allocate(widget, al);

  new_num_columns = (al->width - MARGIN_SIZE)/BOX_SIZE;
  if(new_num_columns != u->num_columns)
    {
      u->num_columns = new_num_columns;
      u->num_rows = NUM_CHARS/u->num_columns;
      gtk_widget_queue_resize(widget);
    }
}

static void
gtk_unipick_paint(GtkWidget *widget, GdkRectangle *area)
{
  GtkUnipick *u = (GtkUnipick *)widget;
  GtkUnipickClass *uc = GTK_UNIPICK_GET_CLASS(u);
  int x, y;
  int startx, endx, starty, endy;
  PangoAnalysis anl;

  if(area->width == widget->allocation.width
     && area->height == widget->allocation.height)
	return; /* Avoid a huge redraw for no reason */

  gdk_window_clear_area (widget->window, area->x, area->y,
			 area->width, area->height);

  startx = x_to_col(u, area->x);
  startx = MAX(startx, 0);
  starty = y_to_row(u, area->y);
  starty = MAX(starty, 0);
  endx = x_to_col(u, area->x + area->width);
  endx = MIN(endx, u->num_columns - 1);
  endy = y_to_row(u, area->y + area->height);
  endy = MIN(endy, u->num_rows - 1);

  if(u->clicked_item.x >= startx
     && u->clicked_item.x <= endx
     && u->clicked_item.y >= starty
     && u->clicked_item.y <= endy)
    gtk_draw_flat_box(widget->style, widget->window, GTK_STATE_ACTIVE,
		      GTK_SHADOW_NONE,
		      col_to_x(u, u->clicked_item.x),
		      row_to_y(u, u->clicked_item.y),
		      BOX_SIZE, BOX_SIZE);

  anl.shape_engine = pango_font_find_shaper(uc->font, "C",
					    g_utf8_get_char("X"));
  anl.lang_engine = NULL;
  anl.font = uc->font;
  anl.level = 0;
  for(y = starty; y <= endy; y++)
    {
      gtk_paint_hline(widget->style, widget->window,
		      GTK_STATE_NORMAL, area, widget,
		      NULL,
		      col_to_x(u, startx),
		      col_to_x(u, endx+1),
		      row_to_y(u, y));
      for(x = startx; x <= endx; x++)
	{
	  PangoGlyphString *gstr;
	  char buf[10];
	  gstr = pango_glyph_string_new();
	  buf[g_unichar_to_utf8(y*u->num_columns + x, buf)] = '\0';
	  pango_shape(buf, strlen(buf), &anl, gstr);
	  gdk_draw_glyphs(widget->window,
			  widget->style->text_gc[GTK_STATE_NORMAL],
			  uc->font,
			  col_to_x(u, x) + SPACE_SIZE,
			  row_to_y(u, y) + SPACE_SIZE + FONT_SIZE,
			  gstr);
	  pango_glyph_string_free(gstr);
	}
    }

  for(x = startx; x <= (endx + 1); x++)
    gtk_paint_vline(widget->style, widget->window,
		    GTK_STATE_NORMAL, area, widget,
		    NULL,
		    row_to_y(u, starty),
		    row_to_y(u, endy+1),
		    col_to_x(u, x));
}

static gboolean
gtk_unipick_expose (GtkWidget *widget, GdkEventExpose *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->expose_event)
    wclass->expose_event(widget, event);

  if(GTK_WIDGET_DRAWABLE(widget))
     gtk_unipick_paint(widget, &event->area);

  return TRUE;
}

static gboolean
gtk_unipick_button_press_event(GtkWidget *widget, GdkEventButton *event)
{
  GtkUnipick *u = (GtkUnipick *)widget;

  if(GTK_WIDGET_CLASS(parent_class)->button_press_event)
    GTK_WIDGET_CLASS(parent_class)->button_press_event(widget, event);

  u->clicked_item.x = x_to_col(u, event->x);
  u->clicked_item.y = y_to_row(u, event->y);
  gtk_widget_queue_draw_area(widget,
			     col_to_x(u, u->clicked_item.x),
			     row_to_y(u, u->clicked_item.y),
			     BOX_SIZE, BOX_SIZE);
  
  return TRUE;
}

static gboolean
gtk_unipick_button_release_event(GtkWidget *widget, GdkEventButton *event)
{
  GtkUnipick *u = (GtkUnipick *)widget;
  guint32 keyval;

  if(GTK_WIDGET_CLASS(parent_class)->button_release_event)
    GTK_WIDGET_CLASS(parent_class)->button_release_event(widget, event);

  gtk_widget_queue_draw_area(widget,
			     col_to_x(u, u->clicked_item.x),
			     row_to_y(u, u->clicked_item.y),
			     BOX_SIZE, BOX_SIZE);

  keyval = (u->clicked_item.x + u->clicked_item.y*u->num_columns);
  gtk_signal_emit(GTK_OBJECT(u), unipick_signals[EMIT_KEY],
		  gdk_unicode_to_keyval(keyval));
  
  u->clicked_item.x = 99;

  return TRUE;
}

GtkWidget *
gtk_unipick_new(void)
{
  return gtk_widget_new(gtk_unipick_get_type(), NULL);
}

static int
row_to_y(GtkUnipick *q, int row)
{
  return MARGIN_SIZE + row*BOX_SIZE;
}

static int
col_to_x(GtkUnipick *q, int col)
{
  return MARGIN_SIZE + col*BOX_SIZE;
}

static int
y_to_row(GtkUnipick *q, int y)
{
  return (y-MARGIN_SIZE)/BOX_SIZE;
}

static int
x_to_col(GtkUnipick *q, int x)
{
  return (x-MARGIN_SIZE)/BOX_SIZE;
}
