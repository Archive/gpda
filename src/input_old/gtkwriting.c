#include "gtkwriting.h"
#include "gtkpeninput.h"
#include <gdk/gdkprivate.h>

static void gtk_writing_input_class_init(GtkWritingInputClass *klass);
static void gtk_writing_input_init(GtkWritingInput *q);
static void gtk_writing_input_destroy(GObject *obj);
static void gtk_writing_input_size_request(GtkWidget *widget, GtkRequisition *req);
static void gtk_writing_input_size_allocate(GtkWidget *widget, GtkAllocation *al);
static void gtk_writing_input_show (GtkWidget *widget);
static void gtk_writing_input_hide (GtkWidget *widget);
static gboolean gtk_writing_input_button_press_event(GtkWidget *widget, GdkEventButton *event);
static gboolean gtk_writing_input_motion_notify_event(GtkWidget *widget, GdkEventMotion *event);
static gboolean gtk_writing_input_button_release_event(GtkWidget *widget, GdkEventButton *event);

GType
gtk_writing_input_get_type(void)
{
  static GType pl_type = 0;
  if(!pl_type)
    {
      static const GTypeInfo pl_info = {
	sizeof(GtkWritingInputClass),
	(GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gtk_writing_input_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (GtkWritingInput),
        0,              /* n_preallocs */
        (GInstanceInitFunc) gtk_writing_input_init,
      };

      pl_type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					"GtkWritingInput",
					&pl_info, 0);
    }

  return pl_type;
}

static GObjectClass *parent_class = NULL;

enum {
  EMIT_KEY,
  LAST_SIGNAL
};
static guint writing_input_signals[LAST_SIGNAL] = { 0 };

static void
gtk_writing_input_class_init(GtkWritingInputClass *klass)
{
  GtkWidgetClass *wclass;
  parent_class = g_type_class_peek_parent(klass);
  G_OBJECT_CLASS(klass)->shutdown = gtk_writing_input_destroy;
  wclass = GTK_WIDGET_CLASS(klass);
  wclass->button_press_event = gtk_writing_input_button_press_event;
  wclass->motion_notify_event = gtk_writing_input_motion_notify_event;
  wclass->button_release_event = gtk_writing_input_button_release_event;
  wclass->size_request = gtk_writing_input_size_request;
  wclass->size_allocate = gtk_writing_input_size_allocate;
  wclass->hide = gtk_writing_input_hide;
  wclass->show = gtk_writing_input_show;

  writing_input_signals[EMIT_KEY] =
    gtk_signal_new("emit_key",
		   GTK_RUN_FIRST,
		   GTK_CLASS_TYPE(wclass),
		   GTK_SIGNAL_OFFSET(GtkWritingInputClass, emit_key),
		   gtk_marshal_VOID__UINT,
		   GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
}

static void
gtk_writing_input_init(GtkWritingInput *q)
{
  GdkGCValues vals;

  /* The magic of X includes a little-utilized feature that lets us
     draw to our heart's content */
  vals.subwindow_mode = GDK_INCLUDE_INFERIORS;
  vals.function = GDK_INVERT;
  gdk_color_white(gdk_colormap_get_system(), &vals.foreground);
  gdk_color_white(gdk_colormap_get_system(), &vals.background);
  q->drawing_gc = gdk_gc_new_with_values(gdk_parent_root,
					 &vals,
					 GDK_GC_SUBWINDOW
					 |GDK_GC_FUNCTION
					 |GDK_GC_FOREGROUND
					 |GDK_GC_BACKGROUND);

  gtk_widget_add_events(GTK_WIDGET(q),
			GDK_BUTTON_MOTION_MASK
			|GDK_BUTTON_PRESS_MASK
			|GDK_BUTTON_RELEASE_MASK);

  q->prev_loc.x = q->prev_loc.y = -1;
}

static void
gtk_writing_input_destroy(GObject *obj)
{
  if(GTK_WIDGET_VISIBLE(obj))
    {
      gdk_pointer_ungrab(GDK_CURRENT_TIME);
      gtk_grab_remove(GTK_WIDGET(obj));
    }
}

static void
gtk_writing_input_size_request(GtkWidget *widget, GtkRequisition *req)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->size_request)
    wclass->size_request(widget, req);
}

static void
gtk_writing_input_size_allocate(GtkWidget *widget, GtkAllocation *al)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->size_allocate)
    wclass->size_allocate(widget, al);
}

static void
gtk_writing_input_show (GtkWidget *widget)
{
  static GdkCursor *grab_cursor = NULL;
  GdkGrabStatus status;
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->show)
    wclass->show(widget);

  if(!grab_cursor)
    grab_cursor = gdk_cursor_new(GDK_PENCIL);
  gtk_grab_add(widget);
  status = gdk_pointer_grab(widget->window, TRUE,
			    GDK_BUTTON_MOTION_MASK
			    |GDK_BUTTON_PRESS_MASK
			    |GDK_BUTTON_RELEASE_MASK, NULL, grab_cursor,
			    GDK_CURRENT_TIME);
  g_return_if_fail(status == GDK_GRAB_SUCCESS);
}

static void
gtk_writing_input_hide (GtkWidget *widget)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);

  if(wclass->hide)
    wclass->hide(widget);

  gdk_pointer_ungrab(GDK_CURRENT_TIME);
  gtk_grab_remove(widget);
}

static gboolean
gtk_writing_input_button_press_event(GtkWidget *widget, GdkEventButton *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkWritingInput *wi = (GtkWritingInput *)widget;

  if(wclass->button_press_event)
    wclass->button_press_event(widget, event);

  if(event->window)
    {
      GtkWidget *evw = NULL;
      gdk_window_get_user_data(event->window, (gpointer *)&evw);
      if(evw && GTK_IS_PEN_INPUT(evw))
	{
	  gdk_pointer_ungrab(GDK_CURRENT_TIME);
	  gtk_grab_remove(widget);
	  gtk_button_clicked(GTK_BUTTON(evw));
	  wi->btn_down = FALSE;
	  return TRUE;
	}
    }

  if(event->button == 1)
    wi->btn_down = TRUE;

  wi->prev_loc.x = event->x_root;
  wi->prev_loc.y = event->y_root;

  return TRUE;
}

static gboolean
gtk_writing_input_motion_notify_event(GtkWidget *widget, GdkEventMotion *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkWritingInput *wi = (GtkWritingInput *)widget;

  if(wclass->motion_notify_event)
    wclass->motion_notify_event(widget, event);

  if(wi->btn_down)
    {
      gdk_draw_line(gdk_parent_root, wi->drawing_gc,
		    wi->prev_loc.x, wi->prev_loc.y,
		    event->x_root, event->y_root);
      wi->prev_loc.x = event->x_root;
      wi->prev_loc.y = event->y_root;
    }

  return TRUE;
}

static gboolean
gtk_writing_input_button_release_event(GtkWidget *widget, GdkEventButton *event)
{
  GtkWidgetClass *wclass = GTK_WIDGET_CLASS(parent_class);
  GtkWritingInput *wi = (GtkWritingInput *)widget;

  if(wclass->button_release_event)
    wclass->button_release_event(widget, event);

  if(event->button == 1)
    wi->btn_down = FALSE;

  return TRUE;
}

GtkWidget *
gtk_writing_input_new(void)
{
  return gtk_widget_new(gtk_writing_input_get_type(), NULL);
}

