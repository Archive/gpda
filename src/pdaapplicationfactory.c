/* GPDA - The Gnome PDA
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * Authors: Alexander Larsson <alexl@redhat.com>
 */

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include "pdaapplication.h"
#include <glib/gtypes.h>
#include <glib/gmacros.h>
#include <gmodule.h>

GList *pda_application_factories = NULL;
GQuark *app_categories = NULL;
gint n_app_categories = 0;;


GdkPixbuf *
pda_application_factory_get_small_icon (PdaApplicationFactory *factory)
{
  g_return_val_if_fail (factory != NULL, NULL);
  
  return (*factory->get_small_icon) (factory);
}

GdkPixbuf *
pda_application_factory_get_large_icon (PdaApplicationFactory *factory)
{
  g_return_val_if_fail (factory != NULL, NULL);
  
  return (*factory->get_large_icon) (factory);
}

PdaApplication *
pda_application_factory_start (PdaApplicationFactory *factory,
			       PdaSessionObject *session_obj,
			       gboolean *reactivated)
{
  PdaApplication *app;

  if (reactivated)
    *reactivated = FALSE;
  
  if (factory->singleton && factory->singleton_instance)
    {
      if (reactivated)
	*reactivated = TRUE;
      return factory->singleton_instance;
    }
  
  app = factory->start (factory, session_obj);
  if (app)
    {
      pda_application_register_running (app);
      if (factory->singleton)
	factory->singleton_instance = app;
    }
  return app;
}

void
register_lib (const gchar *dir, const gchar *name)
{
  GModule *module;
  gchar *path;
  gboolean registred = FALSE;
  pda_register_application_module_func reg;

  path = g_strconcat (dir, G_DIR_SEPARATOR_S, name, NULL);
  
  module = g_module_open (path, 0);

  if (!module)
    g_warning ("Cannot load module %s: %s", path, g_module_error());
  else
    {
      if (module && g_module_symbol (module, "register_application_factory", (gpointer)&reg)) {
	(*reg) ();
	registred = TRUE;
      } else
	g_warning ("module %s doesn't export correct PdaApplication API", path);
    }
  
  g_free (path);
  if (module && !registred)
    g_module_close (module);
}


void
pda_application_factory_register_dir (const gchar *directory)
{
  DIR *dir;
  struct dirent *dent;

  dir = opendir (directory);
  
  if (dir == NULL)
    {
      g_warning ("Cannot scan directory %s\n", directory);
      return;
    }
  
  while ((dent = readdir (dir)))
    {
      int len = strlen (dent->d_name);
      if (len > 3 && strcmp (dent->d_name + len - strlen (".so"), ".so") == 0)
	{
	  register_lib (directory, dent->d_name);
	}
    }
  closedir (dir);
}

/* Called by application factories while loading */
gboolean
pda_application_factory_register (PdaApplicationFactory *factory)
{
  GList *list;

  /* Make sure the app isn't already registred */
  list = pda_application_factories;
  while (list)
    {
      PdaApplicationFactory *other = (PdaApplicationFactory *)list->data;
      if (strcmp (factory->name, other->name)==0)
	{
	  g_warning ("Already registred factory %s", factory->name);
	  return FALSE;
	}

      list = g_list_next (list);
    }

  pda_application_factories = g_list_prepend (pda_application_factories, factory);
  return TRUE;
}

GQuark
pda_app_category_register (const gchar *category_name)
{
  GQuark quark;
  int i;
  gboolean found = FALSE;

  quark = g_quark_from_string (category_name);

  for (i=0;i<pda_n_app_categories;i++)
    {
      if (pda_app_categories[i] == quark)
	{
	  found = TRUE;
	  break;
	}
    }

  if (!found)
    {
      pda_n_app_categories ++;
      pda_app_categories = g_realloc (pda_app_categories, sizeof (GQuark)*pda_n_app_categories);
      pda_app_categories[pda_n_app_categories-1] = quark;
    }
  
  return quark;
}
