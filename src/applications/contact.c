/* GPDA - The Gnome PDA
 * Copyright (C) 2002 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <stdio.h>
#include <pdaapplication.h>
#include <gtk/gtk.h>


#include "contact-pixbufs.h"

typedef struct
{
  gchar *name;
  gchar *contact;
} ContactInfo;

enum
{
  COL_NAME,
  COL_CONTACT,
  NUM_COLUMNS
};

ContactInfo testdata[] = {
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"},
  {"Xavier Ordoquy", "mcarkan@users.sourceforge.net"},
  {"Toto", "toto@neuneu.com"}
};

typedef struct {
  ContactInfo *data;
} ContactAppData;

gboolean
contact_app_quit (PdaApplication* app)
{
  //g_free (((ContactAppData*)app->app_data)->data);
  g_free (app->app_data);
  return TRUE;
}

static GtkTreeModel *
create_model (void)
{
  gint i = 0;
  GtkListStore *store;
  GtkTreeIter iter;

  /* create list store */
  store = gtk_list_store_new (NUM_COLUMNS,
			      G_TYPE_STRING,
			      G_TYPE_STRING);

  /* add data to the list store */
  for (i = 0; i < G_N_ELEMENTS (testdata); i++)
    {
      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter,
			  COL_NAME, testdata[i].name,
			  COL_CONTACT, testdata[i].contact,
			  -1);
    }

  return GTK_TREE_MODEL (store);
}

static void
add_columns (GtkTreeView *treeview)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  //GtkTreeModel *model = gtk_tree_view_get_model (treeview);

  /* column for names */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Names",
						     renderer,
						     "text",
						     COL_NAME,
						     NULL);
  gtk_tree_view_column_set_sort_column_id (column, COL_NAME);
  gtk_tree_view_append_column (treeview, column);

  /* column for contact */
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Contact",
						     renderer,
						     "text",
						     COL_CONTACT,
						     NULL);
  gtk_tree_view_column_set_sort_column_id (column, COL_CONTACT);
  gtk_tree_view_append_column (treeview, column);
}

PdaApplication *
contact_app_start (PdaApplicationFactory *factory,
		   PdaSessionObject *session_obj)
{
  PdaApplication *app;
  ContactAppData *data;
  //gchar buffer[100];
  //static int num = 1;
  GtkWidget *vbox;
  //GtkWidget *entry;
  //GtkWidget *label;
  //GtkWidget *label;
  GtkWidget *sw;
  GtkTreeModel *model;
  GtkWidget *treeview;

  app = pda_application_new (factory);

  g_signal_connect_data (app, "quit", (GCallback)contact_app_quit, NULL, NULL, FALSE);

  data = g_new (ContactAppData, 1);
  data->data = testdata;
  app->app_data = data;

  vbox = gtk_vbox_new (FALSE, 0);

  /*
  sprintf (buffer, "Contact app %d\n", num++);
  label = gtk_label_new (buffer);
  gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

  entry = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), entry, FALSE, FALSE, 0);
  */

  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
				       GTK_SHADOW_ETCHED_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);

  /* create tree model */
  model = create_model ();

  /* create tree view */
  treeview = gtk_tree_view_new_with_model (model);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeview), TRUE);
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (treeview),
				   COL_NAME);

  g_object_unref (G_OBJECT (model));

  gtk_container_add (GTK_CONTAINER (sw), treeview);

  /* add columns to the tree view */
  add_columns (GTK_TREE_VIEW (treeview));

  app->toplevel_widget = vbox;

  gtk_widget_show_all (vbox);

  return app;
}



GdkPixbuf *
contact_get_small_icon (PdaApplicationFactory *factory)
{
  static GdkPixbuf *pix = NULL;

  if (!pix)
    {
      pix = gdk_pixbuf_new_from_inline (sizeof (contact_small), contact_small, TRUE, NULL);
    }
  return gdk_pixbuf_ref (pix);
}

GdkPixbuf *
contact_get_large_icon (PdaApplicationFactory *factory)
{
  static GdkPixbuf *pix = NULL;

  if (!pix)
    {
      pix = gdk_pixbuf_new_from_inline (sizeof (contact_large), contact_large, TRUE, NULL);
    }
  return gdk_pixbuf_ref (pix);
}

PdaApplicationFactory contact_app_factory =
{
  "Contact",
  0,
  FALSE,
  
  contact_get_small_icon,
  contact_get_large_icon,

  contact_app_start
};

void register_application_factory (void)
{
  contact_app_factory.app_category = pda_app_category_register ("applications");
  pda_application_factory_register (&contact_app_factory);
}
