/* GPDA - The Gnome PDA
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * Authors: Alexander Larsson <alexl@redhat.com>
 */

#include "pdaapplication.h"
#include <glib-object.h>
#include <gtk/gtkmarshal.h>

enum {
  SWITCH_TO,
  SWITCH_FROM,
  QUIT,
  SAVE_SESSION,
  LAST_SIGNAL
};

GList *pda_applications_running;

static gpointer parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

extern GList *pda_applications_running;
GQuark *pda_app_categories = NULL;
gint pda_n_app_categories = 0;

static void pda_application_class_init (PdaApplicationClass *klass);
static void pda_application_init       (PdaApplication      *klass);

GType pda_application_get_type (void)
{
  static GType application_type = 0;

  if (!application_type)
    {
      static const GTypeInfo application_info =
      {
	sizeof (PdaApplicationClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) pda_application_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,		/* class_data */
	sizeof (PdaApplication),
	16,			/* n_preallocs */
	(GInstanceInitFunc) pda_application_init,
      };
      
      application_type = g_type_register_static (G_TYPE_OBJECT, "PdaApplication", &application_info, 0);
    }

  return application_type;
}

static void
pda_application_class_init (PdaApplicationClass *klass)
{
  GObjectClass *object_klass = G_OBJECT_CLASS (klass);
  
  parent_class = g_type_class_peek_parent (klass);

  signals[SWITCH_TO] =
    g_signal_new ("switch_to",
		  G_TYPE_FROM_CLASS (object_klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (PdaApplicationClass, switch_to),
		  NULL,
		  NULL,
		  gtk_marshal_VOID__VOID,
		  G_TYPE_NONE,
		  0);

  signals[SWITCH_FROM] =
    g_signal_new ("switch_from",
		  G_TYPE_FROM_CLASS (object_klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (PdaApplicationClass, switch_from),
		  NULL,
		  NULL,
		  gtk_marshal_VOID__VOID,
		  G_TYPE_NONE,
		  0);

  signals[QUIT] =
    g_signal_new ("quit",
		  G_TYPE_FROM_CLASS (object_klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (PdaApplicationClass, quit),
		  NULL,
		  NULL,
		  gtk_marshal_VOID__VOID,
		  G_TYPE_NONE,
		  0);

  signals[SAVE_SESSION] =
    g_signal_new ("save_session",
		  G_TYPE_FROM_CLASS (object_klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (PdaApplicationClass, save_session),
		  NULL,
		  NULL,
		  gtk_marshal_VOID__VOID,
		  G_TYPE_NONE,
		  1,
		  G_TYPE_OBJECT);
  
}

static void
pda_application_init (PdaApplication *klass)
{
}

PdaApplication *
pda_application_new (PdaApplicationFactory *factory)
{
  PdaApplication *app;

  app = g_object_new (pda_application_get_type (), NULL);
  app->factory = factory;
  return app;
}

gchar *
pda_application_get_name (PdaApplication *app)
{
  g_return_val_if_fail (app != NULL, NULL);
  g_return_val_if_fail (PDA_IS_APPLICATION (app), NULL);
  
  return app->factory->name;
}

GQuark
pda_application_get_category (PdaApplication *app)
{
  g_return_val_if_fail (app != NULL, 0);
  g_return_val_if_fail (PDA_IS_APPLICATION (app), 0);
  
  return app->factory->app_category;
}

GdkPixbuf *
pda_application_get_small_icon (PdaApplication *app)
{
  g_return_val_if_fail (app != NULL, NULL);
  g_return_val_if_fail (PDA_IS_APPLICATION (app), NULL);
  
  return (*app->factory->get_small_icon) (app->factory);
}

GdkPixbuf *
pda_application_get_large_icon (PdaApplication *app)
{
  g_return_val_if_fail (app != NULL, NULL);
  g_return_val_if_fail (PDA_IS_APPLICATION (app), NULL);
  
  return (*app->factory->get_large_icon) (app->factory);
}

void
pda_application_quit (PdaApplication *app)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (PDA_IS_APPLICATION (app));

  pda_application_unregister_running (app);
  if (app->factory->singleton &&
      app->factory->singleton_instance == app)
    app->factory->singleton_instance = NULL;
    
  g_signal_emit (G_OBJECT (app), signals[QUIT], 0);
}

void
pda_application_switch_to (PdaApplication   *app)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (PDA_IS_APPLICATION (app));

  g_signal_emit (G_OBJECT (app), signals[SWITCH_TO], 0);
}

void
pda_application_switch_from (PdaApplication   *app)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (PDA_IS_APPLICATION (app));

  g_signal_emit (G_OBJECT (app), signals[SWITCH_FROM], 0);
}

void
pda_application_save_session_to (PdaApplication   *app,
				 PdaSessionObject *session_obj)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (PDA_IS_APPLICATION (app));

  g_signal_emit (G_OBJECT (app), signals[SAVE_SESSION], 0, session_obj);
}

void
pda_application_register_running (PdaApplication *app)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (PDA_IS_APPLICATION (app));

  g_assert (g_list_find (pda_applications_running, app) == NULL);

  pda_applications_running = g_list_prepend (pda_applications_running, app);
}

void
pda_application_unregister_running (PdaApplication   *app)
{
  g_return_if_fail (app != NULL);
  g_return_if_fail (PDA_IS_APPLICATION (app));

  g_assert (g_list_find (pda_applications_running, app));

  pda_applications_running = g_list_remove (pda_applications_running, app);
}

