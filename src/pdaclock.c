#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>

#include "pdaclock.h"

static GtkMiscClass *parent_class = NULL;

static gint pda_clock_expose       (GtkWidget      *widget,
				    GdkEventExpose *event);
static void pda_clock_size_request (GtkWidget      *widget,
				    GtkRequisition *requisition);
static void pda_clock_finalize     (GObject        *object);
static void pda_clock_init         (PdaClock       *clock);
static void pda_clock_class_init   (PdaClockClass  *class);



GtkType
pda_clock_get_type (void)
{
  static GtkType clock_type = 0;
  
  if (!clock_type)
    {
      static const GTypeInfo clock_info =
      {
	sizeof (PdaClockClass),
	NULL,           /* base_init */
	NULL,           /* base_finalize */
	(GClassInitFunc) pda_clock_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data */
	sizeof (PdaClock),
	32,             /* n_preallocs */
	(GInstanceInitFunc) pda_clock_init,
      };

      clock_type = g_type_register_static (GTK_TYPE_MISC,
					   "PdaClock",
					   &clock_info,
					   0);
    }
  
  return clock_type;
}

static void
pda_clock_class_init (PdaClockClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  
  parent_class = gtk_type_class (GTK_TYPE_MISC);
  
  gobject_class->finalize = pda_clock_finalize;

  widget_class->size_request = pda_clock_size_request;
  widget_class->expose_event = pda_clock_expose;
}

static gint
seconds_left_of_minute (void)
{
  struct timeval tv;

  gettimeofday (&tv, NULL);

  return (60 - tv.tv_sec%60);
}

gboolean
pda_clock_timeout (gpointer data)
{
  PdaClock *clock = PDA_CLOCK (data);
  
  clock->timeout_tag = g_timeout_add ((seconds_left_of_minute () + 1)*1000,
				      pda_clock_timeout,
				      clock);
  gtk_widget_queue_clear (GTK_WIDGET (clock));
  return FALSE;
}

static void
pda_clock_init (PdaClock *clock)
{
  GTK_WIDGET_SET_FLAGS (clock, GTK_NO_WINDOW);
  
  clock->layout = NULL;
  clock->mode_24h = 1;
  clock->width = -1;
  clock->height = -1;

  clock->timeout_tag = g_timeout_add ((seconds_left_of_minute ())*1000,
				      pda_clock_timeout,
				      clock);
}


GtkWidget*
pda_clock_new (gboolean mode_24h)
{
  PdaClock *clock;
  
  clock = gtk_type_new (PDA_TYPE_CLOCK);

  clock->mode_24h = mode_24h;
  
  return GTK_WIDGET (clock);
}


static void
pda_clock_finalize (GObject *object)
{
  PdaClock *clock;
  
  g_return_if_fail (PDA_IS_CLOCK (object));
  
  clock = PDA_CLOCK (object);
  
  if (clock->layout)
    g_object_unref (G_OBJECT (clock->layout));

  g_source_remove (clock->timeout_tag);
  
  G_OBJECT_CLASS (parent_class)->finalize (object);
}


static void
pda_clock_size_request (GtkWidget      *widget,
			GtkRequisition *requisition)
{
  PdaClock *clock;
  PangoRectangle logical_rect;
  
  g_return_if_fail (PDA_IS_CLOCK (widget));
  g_return_if_fail (requisition != NULL);
  
  clock = PDA_CLOCK (widget);

  requisition->width = clock->misc.xpad;
  requisition->height = clock->misc.ypad;
  
  if (!clock->layout)
    {
      clock->layout = gtk_widget_create_pango_layout (widget, "");
      pango_layout_set_alignment (clock->layout, PANGO_ALIGN_LEFT);
      pango_layout_set_width (clock->layout, -1);
    }
  
  if (clock->width < 0 || clock->height < 0)
    {
      if (clock->mode_24h)
	pango_layout_set_text (clock->layout, "XX:XX", -1);
      else
	pango_layout_set_text (clock->layout, "XX:XX am", -1);

      pango_layout_get_extents (clock->layout, NULL, &logical_rect);
      clock->width = PANGO_PIXELS (logical_rect.width);
      clock->height = PANGO_PIXELS (logical_rect.height);
    }

  requisition->width += clock->width;
  requisition->height += clock->height;
}

/* Buffer must be at least 9 chars */
static void
pda_clock_generate_clock_string (PdaClock *clock, char *buffer)
{
  struct tm *tm;
  time_t t;
  gint hours, minutes, seconds;

  t = time (NULL);
  tm = localtime (&t);
  
  seconds = tm->tm_sec;
  minutes = tm->tm_min;
  hours = tm->tm_hour;

  if (clock->mode_24h)
    sprintf (buffer, "%2d:%02d", hours, minutes);
  else
    sprintf (buffer, "%2d:%02d %s", hours%12, minutes, (hours>12)?"pm":"am");
}

static gint
pda_clock_expose (GtkWidget      *widget,
		  GdkEventExpose *event)
{
  PdaClock *clock;
  GtkMisc *misc;
  gint x, y;
  gfloat xalign;
  
  g_return_val_if_fail (PDA_IS_CLOCK (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  clock = PDA_CLOCK (widget);

  /* if clock->layout is NULL it means we got a set_text since
   * our last size request, so a resize should be queued,
   * which means a full expose is in the queue anyway.
   */
  if (GTK_WIDGET_VISIBLE (widget) && GTK_WIDGET_MAPPED (widget) && clock->layout)
    {
      char buffer[16];
      misc = GTK_MISC (widget);
      
      if (gtk_widget_get_direction (widget) == GTK_TEXT_DIR_LTR)
	xalign = misc->xalign;
      else
	xalign = 1. - misc->xalign;
      
      x = floor (widget->allocation.x + (gint)misc->xpad
		 + ((widget->allocation.width - widget->requisition.width) * xalign)
		 + 0.5);
      
      y = floor (widget->allocation.y + (gint)misc->ypad 
		 + ((widget->allocation.height - widget->requisition.height) * misc->yalign)
		 + 0.5);

      pda_clock_generate_clock_string (clock, buffer);
      pango_layout_set_text (clock->layout, buffer, -1);

      gtk_paint_layout (widget->style,
                        widget->window,
                        GTK_WIDGET_STATE (widget),
			FALSE,
                        &event->area,
                        widget,
                        "clock",
                        x, y,
                        clock->layout);
    }

  return TRUE;
}
