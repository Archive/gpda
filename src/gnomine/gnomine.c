/*
 *
 * Author:        Pista <szekeres@cyberspace.mht.bme.hu>
 *
 * Score support: horape@compendium.com.ar
 * Mine Resizing: djb@redhat.com
 *
 */
#include <config.h>
#include <gtk/gtk.h>
#include <gtk-clock.h>
#include <pdaapplication.h>
#include <string.h>
#include <stdio.h>

#include "minefield.h"

#include "face-worried.xpm"
#include "face-smile.xpm"
#include "face-cool.xpm"
#include "face-sad.xpm"
#include "face-win.xpm"

#include "pixbufs.h"

static GtkWidget *mfield;
GtkWidget *window;
GtkWidget *flabel;
GtkWidget *xentry;
GtkWidget *yentry;
GtkWidget *mentry;
GtkWidget *sentry;
GtkWidget *mbutton;
GtkWidget *plabel;
GtkWidget *cframe;
GtkWidget *clk;
GtkWidget *pm_win, *pm_sad, *pm_smile, *pm_cool, *pm_worried, *pm_current;
GtkWidget *face_box;
gint ysize=8, xsize=8;
gint nmines=10;
gint fsize=0, fsc;
gint minesize=16;
gboolean clock_running;

#define N_(x) x
#define _(x) x

char *fsize2names[] = {
	N_("Tiny"),
	N_("Medium"),
	N_("Biiiig"),
	N_("Custom"),
};

void show_face(GtkWidget *pm)
{
        if (pm_current == pm) return;

	if (pm_current) {
		gtk_widget_hide(pm_current);
	}

	gtk_widget_show(pm);
	
	pm_current = pm;
}

void quit_game(GtkWidget *widget, gpointer data)
{
	/*gtk_widget_destroy(window);*/
	gtk_main_quit();
}

void set_flabel(GtkMineField *mfield)
{
	char val[16];

	sprintf(val, "%d/%d", mfield->flags, mfield->mcount);
	gtk_label_set(GTK_LABEL(flabel), val);
}

void new_game(GtkWidget *widget, gpointer data)
{
        gtk_clock_stop(GTK_CLOCK(clk));
	clock_running = FALSE;
	gtk_clock_set_seconds(GTK_CLOCK(clk), 0);
        show_face(pm_smile);
	gtk_minefield_restart(GTK_MINEFIELD(mfield));
	gtk_widget_draw(mfield, NULL);
	set_flabel(GTK_MINEFIELD(mfield));
/*        gtk_clock_start(GTK_CLOCK(clk)); */
}

void marks_changed(GtkWidget *widget, gpointer data)
{
        set_flabel(GTK_MINEFIELD(widget));
	gtk_clock_start(GTK_CLOCK(clk));
	clock_running = TRUE;
}

void lose_game(GtkWidget *widget, gpointer data)
{
        show_face(pm_sad);
        gtk_clock_stop(GTK_CLOCK(clk));
	clock_running = FALSE;
}

void win_game(GtkWidget *widget, gpointer data)
{
	clock_running = FALSE;
        gtk_clock_stop(GTK_CLOCK(clk));
        show_face(pm_win);
}

void look_cell(GtkWidget *widget, gpointer data)
{
        show_face(pm_worried);
        gtk_clock_start(GTK_CLOCK(clk));
	clock_running = TRUE;
}

void unlook_cell(GtkWidget *widget, gpointer data)
{
	show_face(pm_cool);
}

void setup_mode(GtkWidget *widget, gint mode)
{
	gint size_table[3][3] = {{ 8, 8, 10 }, {16, 16, 40}, {30, 16, 99}};
	gint x,y,m,s;

	if (mode == 3) {
		x = xsize;
		y = ysize;
		m = nmines;
	} else {
		x = size_table[mode][0];
		y = size_table[mode][1];
		m = size_table[mode][2];
	}
	s = minesize;
	gtk_minefield_set_size(GTK_MINEFIELD(mfield), x, y);
	gtk_minefield_set_mines(GTK_MINEFIELD(mfield), m, s);
}

int range (int val, int min, int max)
{
	if (val < min)
		val = min;
	if (val > max)
		val = max;
	return val;
}

void verify_ranges (void)
{
	minesize = range (minesize, 2, 100);
	xsize    = range (xsize, 4, 100);
	ysize    = range (ysize, 4, 100);
	nmines   = range (nmines, 1, xsize * ysize);
}

void
about(GtkWidget *widget, gpointer data)
{
	/*
        static GtkWidget *about;

        const gchar *authors[] = {
		N_("Code: Pista"),
		N_("Faces: tigert"),
		N_("Score: HoraPe"),
		NULL
	};

	if (about) {
		gdk_window_raise (about->window);
		gdk_window_show (about->window);
		return;
	}

#ifdef ENABLE_NLS
       {
            int i=0;
            while (authors[i] != NULL) { authors[i]=_(authors[i]); i++; }
       }
#endif

        about = gnome_about_new (_("Gnome Mines"), VERSION,
				 _("(C) 1997-1999 the Free Software Foundation"),
				 (const char **)authors,
				 _("Minesweeper clone"),
				 NULL);
	gtk_signal_connect (GTK_OBJECT (about), "destroy", GTK_SIGNAL_FUNC
			(gtk_widget_destroyed), &about);
	gnome_dialog_set_parent (GNOME_DIALOG (about), GTK_WINDOW (window));
        gtk_widget_show (about);
	*/
}



GtkWidget *gnome_pixmap_new_from_xpm_d (char **data)
{
  GdkPixbuf *pixbuf;
  GtkWidget *image;

  pixbuf = gdk_pixbuf_new_from_xpm_data ((const char **)data);
  image = gtk_image_new_from_pixbuf (pixbuf);
  gdk_pixbuf_unref (pixbuf);
  return image;
}

gboolean
gnomines_quit (PdaApplication *app)
{
  return TRUE;
}

gboolean
gnomines_switch_to (PdaApplication *app)
{
  if (clock_running)
    gtk_clock_start(GTK_CLOCK(clk));
  return TRUE;
}

gboolean
gnomines_switch_from (PdaApplication *app)
{
  if (clock_running)
    gtk_clock_stop(GTK_CLOCK(clk));
  return TRUE;
}


PdaApplication *
gnomines_start (PdaApplicationFactory *factory,
		PdaSessionObject *session_obj)
{
        PdaApplication *app;
        GtkWidget *all_boxes;
	GtkWidget *status_table;
	GtkWidget *button_table;
        GtkWidget *align;
        GtkWidget *label;

	app = pda_application_new (factory);
	verify_ranges ();

	clock_running = FALSE;
	
	g_signal_connect_data (app, "quit", G_CALLBACK(gnomines_quit), NULL, NULL, FALSE);
	g_signal_connect_data (app, "switch_to", G_CALLBACK(gnomines_switch_to), NULL, NULL, FALSE);
	g_signal_connect_data (app, "switch_from", G_CALLBACK(gnomines_switch_from), NULL, NULL, FALSE);
	
        all_boxes = gtk_vbox_new(FALSE, 0);

        button_table = gtk_table_new(2, 3, FALSE);
	gtk_box_pack_start(GTK_BOX(all_boxes), button_table, TRUE, TRUE, 0);

        pm_current = NULL;

	mbutton = gtk_button_new();
	gtk_signal_connect(GTK_OBJECT(mbutton), "clicked",
                           GTK_SIGNAL_FUNC(new_game), NULL);
        gtk_widget_set_usize(mbutton, 38, 38);
	gtk_table_attach(GTK_TABLE(button_table), mbutton, 1, 2, 0, 1,
			 0, 0, 5, 5);

	face_box = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(mbutton), face_box);
	
	pm_win     = gnome_pixmap_new_from_xpm_d (face_win_xpm);
	pm_sad     = gnome_pixmap_new_from_xpm_d (face_sad_xpm);
	pm_smile   = gnome_pixmap_new_from_xpm_d (face_smile_xpm);
	pm_cool    = gnome_pixmap_new_from_xpm_d (face_cool_xpm);
	pm_worried = gnome_pixmap_new_from_xpm_d (face_worried_xpm);

        gtk_box_pack_start(GTK_BOX(face_box), pm_win, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(face_box), pm_sad, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(face_box), pm_smile, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(face_box), pm_cool, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(face_box), pm_worried, FALSE, FALSE, 0);

	show_face(pm_smile);

	gtk_widget_show(face_box);
	gtk_widget_show(mbutton);

	gtk_widget_show(button_table);
	
        align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (button_table), align, 1, 2, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0, 0);
        gtk_widget_show (align);
  
	/*
	gtk_widget_push_visual (gdk_imlib_get_visual ());
	gtk_widget_push_colormap (gdk_imlib_get_colormap ());
	*/
        mfield = gtk_minefield_new();
	/*
	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();
	*/
        gtk_container_add (GTK_CONTAINER (align), mfield);

        setup_mode(mfield, fsize);
	
	gtk_signal_connect(GTK_OBJECT(mfield), "marks_changed",
			   GTK_SIGNAL_FUNC(marks_changed), NULL);
	gtk_signal_connect(GTK_OBJECT(mfield), "explode",
			   GTK_SIGNAL_FUNC(lose_game), NULL);
	gtk_signal_connect(GTK_OBJECT(mfield), "win",
			   GTK_SIGNAL_FUNC(win_game), NULL);
	gtk_signal_connect(GTK_OBJECT(mfield), "look",
			   GTK_SIGNAL_FUNC(look_cell), NULL);
	gtk_signal_connect(GTK_OBJECT(mfield), "unlook",
			   GTK_SIGNAL_FUNC(unlook_cell), NULL);
	
	gtk_widget_show(mfield);

        status_table = gtk_table_new(1, 4, TRUE);
	gtk_box_pack_start(GTK_BOX(all_boxes), status_table, TRUE, TRUE, 0);
	label = gtk_label_new(_("Flags:"));
	gtk_table_attach(GTK_TABLE(status_table), label,
			 0, 1, 0, 1, 0, 0, 3, 3);
	gtk_widget_show(label);
	
	flabel = gtk_label_new("0");
	
	gtk_table_attach(GTK_TABLE(status_table), flabel,
			 1, 2, 0, 1, 0, 0, 3, 3);
	gtk_widget_show(flabel);

	label = gtk_label_new(_("Time:"));
	gtk_table_attach(GTK_TABLE(status_table), label,
			 2, 3, 0, 1, 0, 0, 3, 3);
	gtk_widget_show(label);

        clk = gtk_clock_new(GTK_CLOCK_INCREASING);
	gtk_table_attach(GTK_TABLE(status_table), clk,
		3, 4, 0, 1, 0, 0, 3 ,3);
	gtk_widget_show(clk);
	
        gtk_widget_show(status_table);
	
	new_game(mfield, NULL);
	
	app->toplevel_widget = all_boxes;

	return app;
}


GdkPixbuf *
gnomines_get_small_icon (PdaApplicationFactory *factory)
{
  static GdkPixbuf *pix = NULL;

  if (!pix)
    {
      pix = gdk_pixbuf_new_from_inline (sizeof (gnomine_small), gnomine_small, TRUE, NULL);
    }
  return gdk_pixbuf_ref (pix);
}

GdkPixbuf *
gnomines_get_large_icon (PdaApplicationFactory *factory)
{
  static GdkPixbuf *pix = NULL;

  if (!pix)
    {
      pix = gdk_pixbuf_new_from_inline (sizeof (gnomine_small), gnomine_small, TRUE, NULL);
    }
  return gdk_pixbuf_ref (pix);
}


PdaApplicationFactory gnomines_factory =
{
  "GnoMines",
  0,
  TRUE,

  gnomines_get_small_icon,
  gnomines_get_large_icon,

  gnomines_start
};

void register_application_factory (void)
{
  gnomines_factory.app_category = pda_app_category_register ("Games");
  pda_application_factory_register (&gnomines_factory);
}
