#include <gtk/gtkmisc.h>

#define PDA_TYPE_CLOCK		  (pda_clock_get_type ())
#define PDA_CLOCK(obj)		  (GTK_CHECK_CAST ((obj), PDA_TYPE_CLOCK, PdaClock))
#define PDA_CLOCK_CLASS(klass)	  (GTK_CHECK_CLASS_CAST ((klass), PDA_TYPE_CLOCK, PdaClockClass))
#define PDA_IS_CLOCK(obj)	  (GTK_CHECK_TYPE ((obj), PDA_TYPE_CLOCK))
#define PDA_IS_CLOCK_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), PDA_TYPE_CLOCK))
#define PDA_CLOCK_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), PDA_TYPE_CLOCK, PdaClockClass))

typedef struct _PdaClock PdaClock;
typedef struct _PdaClockClass PdaClockClass;


struct _PdaClock
{
  GtkMisc misc;

  PangoLayout *layout;
  gint width, height;

  guint timeout_tag;
  
  gboolean mode_24h : 1;
};

struct _PdaClockClass
{
  GtkMiscClass parent_class;
};


GtkType    pda_clock_get_type       (void) G_GNUC_CONST;
GtkWidget *pda_clock_new            (gboolean mode_24h);

void pda_clock_set_mode (gboolean mode_24h);
