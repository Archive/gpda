/* GPDA - The Gnome PDA
 * Copyright (C) 2002 Xavier Ordoquy
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include <gdk/gdk.h>
#include <gtk/gtk.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define PDA_TYPE_PANEL            (pda_panel_get_type ())
#define PDA_PANEL(obj)            (GTK_CHECK_CAST ((obj),         PDA_TYPE_PANEL, PdaPanel))
#define PDA_PANEL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), PDA_TYPE_PANEL, PdaPanelClass))
#define PDA_IS_PANEL(obj)         (GTK_CHECK_TYPE ((obj),         PDA_TYPE_PANEL))
#define PDA_IS_PANEL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), PDA_TYPE_PANEL))
#define PDA_PANEL_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj),    PDA_TYPE_PANEL, PdaPanelClass))


typedef struct _PdaPanel	      PdaPanel;
typedef struct _PdaPanelClass   PdaPanelClass;
//typedef struct _PdaPanelChild   PdaPanelChild;

struct _PdaPanel
{
  GtkContainer container;
  
  GList *children;

  GtkWidget *goleft;
  GtkWidget *goright;
};

struct _PdaPanelClass
{
  GtkContainerClass parent_class;
};

GType	   pda_panel_get_type (void) G_GNUC_CONST;
GtkWidget* pda_panel_new	         ();

void	   pda_panel_pack_start	         (PdaPanel       *panel,
					  GtkWidget      *child);
void       pda_panel_clean               (PdaPanel       *panel);

#ifdef __cplusplus
}
#endif /* __cplusplus */
