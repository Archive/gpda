/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GTK+ Team and others 1997-2000.  See the AUTHORS
 * file for a list of people on the GTK+ Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GTK+ at ftp://ftp.gtk.org/pub/gtk/. 
 */

#include <string.h>
#include "pdaappcontainer.h"

static void pda_app_container_class_init                    (PdaAppContainerClass *klass);
static void pda_app_container_init                          (PdaAppContainer      *container);
static void pda_app_container_finalize                      (GObject              *object);
static void pda_app_container_paint                         (GtkWidget            *widget,
							     GdkRectangle         *area);
static gint pda_app_container_expose                        (GtkWidget            *widget,
							     GdkEventExpose       *event);
static void pda_app_container_size_request                  (GtkWidget            *widget,
							     GtkRequisition       *requisition);
static void pda_app_container_size_allocate                 (GtkWidget            *widget,
							     GtkAllocation        *allocation);
/*
static void pda_app_container_remove                        (GtkContainer         *container,
							     GtkWidget            *child);
*/
static void pda_app_container_compute_child_allocation      (PdaAppContainer      *container,
							     GtkAllocation        *child_allocation);
static void pda_app_container_real_compute_child_allocation (PdaAppContainer      *container,
							     GtkAllocation        *child_allocation);

static GtkContainerClass *parent_class = NULL;

GtkType
pda_app_container_get_type (void)
{
  static GtkType app_container_type = 0;

  if (!app_container_type)
    {
      static const GtkTypeInfo app_container_info =
      {
	"PdaAppContainer",
	sizeof (PdaAppContainer),
	sizeof (PdaAppContainerClass),
	(GtkClassInitFunc) pda_app_container_class_init,
	(GtkObjectInitFunc) pda_app_container_init,
        /* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };

      app_container_type = gtk_type_unique (gtk_bin_get_type (), &app_container_info);
    }

  return app_container_type;
}

static void
pda_app_container_class_init (PdaAppContainerClass *class)
{
  GObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;

  object_class = G_OBJECT_CLASS (class);
  widget_class = GTK_WIDGET_CLASS (class);
  container_class = GTK_CONTAINER_CLASS (class);

  parent_class = gtk_type_class (gtk_bin_get_type ());

  object_class->finalize = pda_app_container_finalize;
  
  widget_class->expose_event = pda_app_container_expose;
  widget_class->size_request = pda_app_container_size_request;
  widget_class->size_allocate = pda_app_container_size_allocate;

  class->compute_child_allocation = pda_app_container_real_compute_child_allocation;
}

static void
pda_app_container_init (PdaAppContainer *container)
{
}

static void
pda_app_container_finalize (GObject *object)
{
  PdaAppContainer *container = PDA_APP_CONTAINER (object);
  GList *apps;

  apps = container->apps;
  while (apps)
    {
      PdaApplication *app = apps->data;
      apps = g_list_next (apps);

      gtk_widget_unref (app->toplevel_widget);
      g_object_unref (G_OBJECT (app));
    }

  g_list_free (container->apps);
  container->apps = NULL;
}

GtkWidget*
pda_app_container_new ()
{
  PdaAppContainer *container;

  container = gtk_type_new (pda_app_container_get_type ());

  return GTK_WIDGET (container);
}

static void
pda_app_container_paint (GtkWidget    *widget,
			 GdkRectangle *area)
{
  PdaAppContainer *container;
  gint x, y, width, height;

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      container = PDA_APP_CONTAINER (widget);

      x = container->child_allocation.x - widget->style->xthickness;
      y = container->child_allocation.y - widget->style->ythickness;
      width = container->child_allocation.width + 2 * widget->style->xthickness;
      height =  container->child_allocation.height + 2 * widget->style->ythickness;

      gtk_paint_shadow (widget->style, widget->window,
			GTK_STATE_NORMAL, GTK_SHADOW_IN,
			area, widget, "app",
			x, y, width, height);
    }
}

static gboolean
pda_app_container_expose (GtkWidget      *widget,
			  GdkEventExpose *event)
{
  PdaAppContainer *container = PDA_APP_CONTAINER (widget);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      pda_app_container_paint (widget, &event->area);
      
      if (container->cur_app && GTK_WIDGET_NO_WINDOW (container->cur_app->toplevel_widget))
	gtk_container_propagate_expose (GTK_CONTAINER (widget),
					container->cur_app->toplevel_widget,
					event);
    }

  return FALSE;
}

static void
pda_app_container_size_request (GtkWidget      *widget,
				GtkRequisition *requisition)
{
  GtkBin *bin = GTK_BIN (widget);
  GtkRequisition child_requisition;
  
  requisition->width = 0;
  requisition->height = 0;
  
  if (bin->child && GTK_WIDGET_VISIBLE (bin->child))
    {
      gtk_widget_size_request (bin->child, &child_requisition);

      requisition->width = MAX (requisition->width, child_requisition.width);
      requisition->height += child_requisition.height;
    }

  requisition->height += (GTK_CONTAINER (widget)->border_width +
			  GTK_WIDGET (widget)->style->ythickness) * 2;
}

static void
pda_app_container_size_allocate (GtkWidget     *widget,
				 GtkAllocation *allocation)
{
  PdaAppContainer *container = PDA_APP_CONTAINER (widget);
  GtkBin *bin = GTK_BIN (widget);
  GtkAllocation new_allocation;

  widget->allocation = *allocation;

  pda_app_container_compute_child_allocation (container, &new_allocation);
  
  /* If the child allocation changed, that means that the frame is drawn
   * in a new place, so we must redraw the entire widget.
   */
  if (GTK_WIDGET_MAPPED (widget) &&
      (new_allocation.x != container->child_allocation.x ||
       new_allocation.y != container->child_allocation.y ||
       new_allocation.width != container->child_allocation.width ||
       new_allocation.height != container->child_allocation.height))
    gtk_widget_queue_clear (widget);
  
  if (bin->child && GTK_WIDGET_VISIBLE (bin->child))
    gtk_widget_size_allocate (bin->child, &new_allocation);
  
  container->child_allocation = new_allocation;
}

static void
pda_app_container_compute_child_allocation (PdaAppContainer      *container,
					    GtkAllocation *child_allocation)
{
  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_CONTAINER (container));
  g_return_if_fail (child_allocation != NULL);

  PDA_APP_CONTAINER_GET_CLASS (container)->compute_child_allocation (container, child_allocation);
}

static void
pda_app_container_real_compute_child_allocation (PdaAppContainer      *container,
						 GtkAllocation *child_allocation)
{
  GtkWidget *widget = GTK_WIDGET (container);
  GtkAllocation *allocation = &widget->allocation;
  gint top_margin;

  top_margin = widget->style->ythickness;
  
  child_allocation->x = GTK_CONTAINER (container)->border_width;
  child_allocation->width = MAX(1, (gint)allocation->width - child_allocation->x * 2);
  
  child_allocation->y = (GTK_CONTAINER (container)->border_width + top_margin);
  child_allocation->height = MAX (1, ((gint)allocation->height - child_allocation->y -
				      (gint)GTK_CONTAINER (container)->border_width -
				      (gint)widget->style->ythickness));
  
  child_allocation->x += allocation->x;
  child_allocation->y += allocation->y;
}

PdaApplication *
pda_app_container_current  (PdaAppContainer *container)
{
  return container->cur_app;
}

void
pda_app_container_add_app (PdaAppContainer *container,
			   PdaApplication *app)
{
  g_return_if_fail (PDA_IS_APPLICATION (app));
  g_return_if_fail (!g_list_find (container->apps, app));

  g_object_ref (G_OBJECT (app));
  gtk_widget_ref (app->toplevel_widget);
  
  container->apps = g_list_prepend (container->apps, app);

  if (container->cur_app == NULL)
    pda_app_container_switch_to_app (container, app);
}

void
pda_app_container_remove_app (PdaAppContainer *container,
			      PdaApplication *app)
{
  g_return_if_fail (PDA_IS_APPLICATION (app));
  g_return_if_fail (g_list_find (container->apps, app));

  gtk_widget_unref (app->toplevel_widget);
  g_object_unref (G_OBJECT (app));

  container->apps = g_list_remove (container->apps, app);

  if (container->cur_app == app)
    {
      PdaApplication *new_app = NULL;
      if (container->apps)
        new_app = (PdaApplication *)container->apps->data;
      pda_app_container_switch_to_app (container, new_app);
    }
}

void
pda_app_container_switch_to_app (PdaAppContainer *container,
				 PdaApplication *app)
{
  g_return_if_fail (container != NULL);
  g_return_if_fail (PDA_IS_APP_CONTAINER (container));

  if (container->cur_app == app)
    return;

  if (container->cur_app)
    {
      pda_application_switch_from (container->cur_app);
      if (GTK_WIDGET_VISIBLE (container->cur_app->toplevel_widget))
	gtk_widget_hide (container->cur_app->toplevel_widget);
      
      gtk_container_remove (GTK_CONTAINER (container),
			    container->cur_app->toplevel_widget);
    }

  container->cur_app = app;

  if (app)
    {
      if (GTK_WIDGET_VISIBLE (container))
	gtk_widget_show (container->cur_app->toplevel_widget);
      
      gtk_container_add (GTK_CONTAINER (container),
			 container->cur_app->toplevel_widget);
      pda_application_switch_to (container->cur_app);
    }
  
  gtk_widget_queue_resize (GTK_WIDGET (container));
}
