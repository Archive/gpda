/* GPDA - The Gnome PDA
 * Copyright (C) 2001 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/* 
 * Authors: Alexander Larsson <alexl@redhat.com>
 */

#include "pdacommands.h"
#include "pdaui.h"

void
pda_start_app (PdaApplicationFactory *factory)
{
  PdaApplication *app;
  gboolean reactivated;

  app = pda_application_factory_start (factory,
				       NULL,
				       &reactivated);

  if (app)
    {
      if (!reactivated)
	{
	  pda_app_container_add_app (pda_app_container, app);
	  pda_ui_update_running_apps ();
	}
      pda_app_container_switch_to_app (pda_app_container, app);
    }
}

void
pda_quit_app (PdaApplication *app)
{
  pda_app_container_remove_app (pda_app_container, app);
  pda_application_quit (app);
  pda_ui_update_running_apps ();
  g_object_unref (G_OBJECT (app));
}
